This repository is associated with the paper  "Aeroelastic Verification of a Dynamic Aerothermoelastic Analysis Framework," presented at the
2019 AIAA Dynamics Specialists Conference. Refer to the paper for details on implementation and function. 

Joe Schoneman
4 December 2018
joe.schoneman@ata-e.com
or
jdschoneman@gmail.com

...


abaqus_data: Contains model input files and results for Abaqus solutions. Analysis/step input files for explicit integration using
             user subroutines are included in full, as are the VDLOAD subroutines themselves. The step input files for cosimulation 
             analyses are not included; the CSE implementation is complex and proprietary.


matlab_data: Contains integration results for MATLAB-based nonlinear reduced order models. 

python_data: Contains integration and stability results for the Python-based Galerkin models. Most of these are not uploaded due to file size;
             running them again using "galerkin/runIntegration.py" will populate the directory.

galerkin: Contains the Galerkin Python implementation and scripts to perform integrations and stability calculations.

figures: Generates and stores all figures used for the paper. In some cases, simple calculations are performed in each figure script.
         In other cases, pre-computed data is loaded to create the plots.

******************************************************
            COPYRIGHT  (C)  2019            
           BY ATA ENGINEERING, INC.         
             ALL RIGHTS RESERVED            


Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
******************************************************
