REM Writes out a panel model with the desired H/h and other properties.

REM Set appropriate path to this top level directory before calling script.
set PYWRITE=<TOP_LEVEL_DIR>/model_writing/writeCurvedPanel.py
set Hh=3.5
python %PYWRITE% -filename model.inp -E 7.100E+10 -t 1.000E-03 -nu 3.300E-01 -rho 2.700E+03 -Hh %Hh% -Lx 5.000E-01 -Ly 2.500E-02 -Nex 80 -Ney 4 -xi 7.500E-01
pause