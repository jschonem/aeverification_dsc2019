# -*- coding: utf-8 -*-
"""

Program to write out supersonic aerodynamic approximations for verification with analytical
flutter/postflutter results.

"""

from numpy import sqrt as sqrt
import argparse
import code

# Handle input arguments
parser = argparse.ArgumentParser(description = 'Write an approximate supersonic/hypesonic aero VDLOAD routine.')
parser.add_argument('-filename', help = 'File to write', required = False, default = 'VDLOAD.f', type = str)
parser.add_argument('-mum', help = 'mu/M', type = float)
parser.add_argument('-lamb', help = 'lambda', type = float)
parser.add_argument('-minf', help = 'mInf', type = float)
parser.add_argument('-a', help = 'a', type = float)
parser.add_argument('-t', help = 't', type = float)
parser.add_argument('-rho', help = 'rho', type = float)
parser.add_argument('-E', help = 'E', type = float)
parser.add_argument('-nu', help = 'nu', type = float)
parser.add_argument('-gamma', help = 'gamma', required = False, default = 1.4, type = float)
parser.add_argument('-approx', help = 'Type of approximation; PTA3 or QS1', default = '3OP',
                    required = False, type = str)

args = parser.parse_args()

filename = args.filename
muM = args.mum
lamb = args.lamb
mInf = args.minf
gamma = args.gamma
a = args.a
t = args.t
rho = args.rho
E = args.E
nu = args.nu
approx = args.approx

D = E*t*t*t/(12*(1 - nu*nu))


def setpInf(lamb, mInf, gamma, a, D):
    """
    Set the required static pressure to obtain a desired lambda given the freestream Mach number,
    specific heat ratio gamma, panel length a, and panel rigidity D.
    """

    return lamb * sqrt(mInf*mInf - 1)/(mInf*mInf) * (D/(gamma*a*a*a))


def setaInf(muM, pInf, mInf, a, gamma, rhoM, t):
    """
    Set the freestream speed of sound to obtain a desired rati of mu/M.
    """

    return sqrt(1/muM * (pInf/mInf) * (a*gamma/(rhoM*t)))


# Obtain parameters of interest
pInf = setpInf(lamb, mInf, gamma, a, D)
aInf = setaInf(muM, pInf, mInf, a, gamma, rho, t)

# Write out the VDLOAD file
if approx == '3OP':
    data = "      subroutine vdload (\n" + \
        "C Read only (unmodifiable)variables -\n" + \
        "     * nBlock, ndim, stepTime, totalTime,\n" + \
        "     * amplitude, curCoords, velocity, dirCos, jltyp, sname,\n" + \
        "C Write only (modifiable) variable -\n" + \
        "     * value )\n" + \
        "      include 'vaba_param.inc'\n" + \
        "      parameter (one = 1.0d0,\n" + \
        "     *           gamm = %fd0,\n" % gamma + \
        "     *           mInf = %fd0,\n" % mInf + \
        "     *           aInf = %fd0,\n" % aInf + \
        "     *           vhat1 = 1.0d0,\n" + \
        "     *           vhat2 = 0.0d0,\n" + \
        "     *           vhat3 = 0.0d0,\n" + \
        "     *           pInf = %fd0)\n" % pInf + \
        "      dimension curCoords(nBlock,ndim), velocity(nBlock,ndim),\n" + \
        "     *  dirCos(nBlock,ndim,ndim), value(nBlock)\n" + \
        "      character*80 sname\n" + \
        "C Values defined in terms of other parameters\n" + \
        "      REAL(8) :: gp14, oneThird, nDotv, dydx, vn, cp, aInfi\n" + \
        "      REAL(8), DIMENSION(3) :: P\n" + \
        "      gp14 = (gamm + one)/4.0d0\n" + \
        "      oneThird = one/3.0d0\n" + \
        "      aInfi = one/aInf\n" + \
        "C Main evaluation loop.\n" + \
        "      do k = 1, nBlock\n" + \
        "C Obtain the dot product between local normal and velocity unit vector.\n" + \
        "          nDotv = vhat1*dirCos(k, 3, 1)\n" + \
        "          nDotv = nDotv + vhat2*dirCos(k, 3, 2)\n" + \
        "          nDotV = nDotv + vhat3*dirCos(k, 3, 3)\n" + \
        "C Obtain the local slope.\n" + \
        "          dydx = -nDotv/sqrt(one - nDotv*nDotv)\n" + \
        "C Obtain component normal to freestream velocity\n" + \
        "         P(1) = dirCos(k, 3, 1) - nDotv*vhat1\n" + \
        "         P(2) = dirCos(k, 3, 2) - nDotv*vhat2\n" + \
        "         P(3) = dirCos(k, 3, 3) - nDotv*vhat3\n" + \
        "         vn = sqrt(P(1)*P(1) + P(2)*P(2) + P(3)*P(3))\n" + \
        "      vn=(P(1)*velocity(k,1)+P(2)*velocity(k,2))+P(3)*velocity(k,3)/vn\n" + \
        "C Define local normal velocity (vertical)\n" + \
        "          vn = mInf*dydx + vn*aInfi\n" + \
        "C Obtain the pressure coefficient and assign to value\n" + \
        "          cp = gamm*vn*(one + gp14*vn*(one + oneThird*vn))\n" + \
        "          value(k) = pInf*cp\n" + \
        "      end do\n" + \
        "      return\n" + \
        "      end\n"

elif approx == 'QS1':
    data = "      subroutine vdload (\n" + \
        "C Read only (unmodifiable)variables -\n" + \
        "     * nBlock, ndim, stepTime, totalTime,\n" + \
        "     * amplitude, curCoords, velocity, dirCos, jltyp, sname,\n" + \
        "C Write only (modifiable) variable -\n" + \
        "     * value )\n" + \
        "      include 'vaba_param.inc'\n" + \
        "      parameter (one = 1.0d0,\n" + \
        "     *           two = 2.0d0,\n" + \
        "     *           gamm = %fd0,\n" % gamma + \
        "     *           mInf = %fd0,\n" % mInf + \
        "     *           aInf = %fd0,\n" % aInf + \
        "     *           vhat1 = 1.0d0,\n" + \
        "     *           vhat2 = 0.0d0,\n" + \
        "     *           vhat3 = 0.0d0,\n" + \
        "     *           pInf = %fd0)\n" % pInf + \
        "      dimension curCoords(nBlock,ndim), velocity(nBlock,ndim),\n" + \
        "     *  dirCos(nBlock,ndim,ndim), value(nBlock)\n" + \
        "      character*80 sname\n" + \
        "C Values defined in terms of other parameters\n" + \
        "      REAL(8) :: gp14, oneThird, nDotv, dydx, vn, cp, aInfi\n" + \
        "      REAL(8), DIMENSION(3) :: P\n" + \
        "      gp14 = (gamm + one)/4.0d0\n" + \
        "      oneThird = one/3.0d0\n" + \
        "      aInfi = one/aInf\n" + \
        "C Main evaluation loop.\n" + \
        "      do k = 1, nBlock\n" + \
        "C Obtain the dot product between local normal and velocity unit vector.\n" + \
        "          nDotv = vhat1*dirCos(k, 3, 1)\n" + \
        "          nDotv = nDotv + vhat2*dirCos(k, 3, 2)\n" + \
        "          nDotV = nDotv + vhat3*dirCos(k, 3, 3)\n" + \
        "C Obtain the local slope.\n" + \
        "          dydx = -nDotv/sqrt(one - nDotv*nDotv)\n" + \
        "C Obtain component normal to freestream velocity\n" + \
        "         P(1) = dirCos(k, 3, 1) - nDotv*vhat1\n" + \
        "         P(2) = dirCos(k, 3, 2) - nDotv*vhat2\n" + \
        "         P(3) = dirCos(k, 3, 3) - nDotv*vhat3\n" + \
        "         vn = sqrt(P(1)*P(1) + P(2)*P(2) + P(3)*P(3))\n" + \
        "      vn=(P(1)*velocity(k,1)+P(2)*velocity(k,2))+P(3)*velocity(k,3)/vn\n" + \
        "C Obtain the pressure coefficient and assign to value\n" + \
        "          cp = dydx + (mInf*mInf - two)/(mInf*mInf - one)/(mInf*aInf)*vn\n" + \
        "          value(k) = pInf*gamm*mInf*mInf/sqrt(mInf*mInf - one)*cp\n" + \
        "      end do\n" + \
        "      return\n" + \
        "      end\n"
else:
    raise ValueError('Incorrect type of approximation %s supplied' % approx)
with open(filename, 'wt') as fid:
    fid.write(data)