#!/bin/bash
#
# File to run a set of dynamic time integrations using Abaqus/Explicit. Before using:
#
# 1) Make a directory to hold this file and the actual analysis.
# 2) Copy this file into the new directory.
# 3) In the new directory, make an empty "results" directory to store the saved output of the analysis.
# 
# The end result will be a set of Python pickle files which hold the response at the 3/4 point of the panel.

# First, write the Abaqus model for the desired conditions
t=0.001
rho=2700.0
E=71E9
nu=0.33
Hh=5.0
Lx=0.5
Ly=0.025
Nex=80
Ney=4
xi=0.75

ABWRITE=$HOME/virtual/explicitVDLOAD/writeCurvedPanel.py

python ${ABWRITE} -filename model.inp -E ${E} -t ${t} -nu ${nu} -rho ${rho} -Hh ${Hh} -Lx ${Lx} -Ly ${Ly} -Nex ${Nex} -Ney ${Ney} -xi ${xi}

# Copy over master files from directory above
cp ../explicit.inp explicit.inp
cp ../runCase runCase
# cp ../postprocess postprocess
cp ../abaqus_v6.env abaqus_v6.env

# for lambda in 50 100 150 200 250 300 350 400 500 600 700 800 900 1000
for lambda in 100 400
do
	# Run Analysis
	./runCase ${lambda}
	
	# Run post-processing
	../postprocess ${lambda}
	
done

# Get rid of the abaqus_v6 file in this directory to avoid messing up windows
# CAE launch.
rm abaqus_v6.env
