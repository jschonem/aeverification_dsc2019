      subroutine vdload (
C Read only (unmodifiable)variables -
     * nBlock, ndim, stepTime, totalTime,
     * amplitude, curCoords, velocity, dirCos, jltyp, sname,
C Write only (modifiable) variable -
     * value )
      include 'vaba_param.inc'
      parameter (one = 1.0d0,
     *           two = 2.0d0,
     *           gamm = 1.400000d0,
     *           mInf = 10.000000d0,
     *           aInf = 31.284718d0,
     *           vhat1 = 1.0d0,
     *           vhat2 = 0.0d0,
     *           vhat3 = 0.0d0,
     *           pInf = 3775.115189d0)
      dimension curCoords(nBlock,ndim), velocity(nBlock,ndim),
     *  dirCos(nBlock,ndim,ndim), value(nBlock)
      character*80 sname
C Values defined in terms of other parameters
      REAL(8) :: gp14, oneThird, nDotv, dydx, vn, cp, aInfi
      REAL(8), DIMENSION(3) :: P
      gp14 = (gamm + one)/4.0d0
      oneThird = one/3.0d0
      aInfi = one/aInf
C Main evaluation loop.
      do k = 1, nBlock
C Obtain the dot product between local normal and velocity unit vector.
          nDotv = vhat1*dirCos(k, 3, 1)
          nDotv = nDotv + vhat2*dirCos(k, 3, 2)
          nDotV = nDotv + vhat3*dirCos(k, 3, 3)
C Obtain the local slope.
          dydx = -nDotv/sqrt(one - nDotv*nDotv)
C Obtain component normal to freestream velocity
         P(1) = dirCos(k, 3, 1) - nDotv*vhat1
         P(2) = dirCos(k, 3, 2) - nDotv*vhat2
         P(3) = dirCos(k, 3, 3) - nDotv*vhat3
         vn = sqrt(P(1)*P(1) + P(2)*P(2) + P(3)*P(3))
      vn=(P(1)*velocity(k,1)+P(2)*velocity(k,2))+P(3)*velocity(k,3)/vn
C Obtain the pressure coefficient and assign to value
          cp = dydx + (mInf*mInf - two)/(mInf*mInf - one)/(mInf*aInf)*vn
          value(k) = pInf*gamm*mInf*mInf/sqrt(mInf*mInf - one)*cp
      end do
      return
      end
