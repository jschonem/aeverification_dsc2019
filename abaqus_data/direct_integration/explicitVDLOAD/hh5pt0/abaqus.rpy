# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2016 replay file
# Internal Version: 2015_09_24-15.31.09 126547
# Run by jschonem on Thu Aug 09 16:02:12 2018
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=357.540618896484, 
    height=122.317497253418)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
#: Executing "onCaeStartup()" in the home directory ...
o2 = session.openOdb(name='explicit')
#* OdbError: Cannot open file S:/virtual/explicitVDLOAD/hh5pt0/explicit. 
#* ***ERROR: "S:/virtual/explicitVDLOAD/hh5pt0/explicit"  is not an Abaqus 
#* database file.
