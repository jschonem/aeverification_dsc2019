# -*- coding: utf-8 -*-
"""

Script to load up a panel response time history, obtain peak amplitude over some final portion of
the response, and plot the time-varying amplitudes for all nodes in the time history.


"""

import argparse
import pickle
import numpy as np
# import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description = 'Load a time history, get peak amplitude, and plot amplitudes.')
parser.add_argument('-filename', help = 'Pickle file to load (with extension)', required = True, type = str)
parser.add_argument('-save', help = 'Path/name to save image and pickle data', required = True, type = str)
parser.add_argument('-time', help = 'Portion of time to check/plot', required = True, type = float)
parser.add_argument('-t', help = 'Panel thickness', required = True, type = float)
parser.add_argument('-lamb', help = 'ND Dynamic Pressure', required = True, type = float)

args = parser.parse_args()

filename = args.filename
time = args.time
thick = args.t
plotname = args.save + '.png'
picklename = args.save + '.pkl'

with open(filename, 'rb') as fid:
    data = pickle.load(fid)

data.pop('start_time')

keys = list(data.keys())
nKeys = len(keys)

t = data[keys[0]][:, 0]
T = t[-1]
tInd = np.where(t > (T - time))[0][0]
t = t[tInd:] - t[tInd]

nt = len(t)

# fig = plt.figure(figsize = (14, 6))
labels = list()
responses = np.zeros((nt, nKeys))
peakVals = np.zeros((nKeys, ))

for i, key in enumerate(keys):
    responses[:, i] = data[key][tInd:, 1]
    nodeNum = key.split('.')[-1].split('_')[0]
    peakInd = np.argmax(np.abs(responses[:, i]))
    peakVals[i] = responses[peakInd, i]
    labels.append(nodeNum)
    # plt.plot(t, responses[:, i]/thick, label = 'Node %s' % nodeNum)

# plt.legend(fontsize = 13)

# Format figure for saving
# plt.xlim((t[0], t[-1]))
# plt.xlabel('Time [s]', fontsize = 14, fontweight = 'bold')
# plt.ylabel('w/t [-]', fontsize = 14, fontweight = 'bold')

# plt.title('Final %.2f Seconds of Response; $\mathbf{\lambda=%.0f}$' % (time, args.lamb), fontsize = 14,
#          fontweight = 'bold')

# plt.savefig(plotname, transparent = True, bbox_inches = 'tight')

# Format data for saving
saveDict = {'responses': responses, 'peakVals': peakVals, 'labels': labels, 't': t, 'thick': thick,
            'lambda': args.lamb}

with open(picklename, 'wb') as fid:
    pickle.dump(saveDict, fid)

