# -*- coding: utf-8 -*-
"""

Program to write out a curved panel to the desired shape using a given rise/thickness ratio.

"""

import numpy as np
import argparse

# Handle input arguments
parser = argparse.ArgumentParser(description = 'Write an approximate supersonic/hypesonic aero VDLOAD routine.')
parser.add_argument('-filename', help = 'File to write', required = False, default = 'model.inp', type = str)
parser.add_argument('-Hh', help = 'Rise ratio H/h', type = float)
parser.add_argument('-E', help = 'Modulus of elasticity', type = float)
parser.add_argument('-t', help = 'Thickness', type = float)
parser.add_argument('-nu', help = 'Poissons ratio', type = float)
parser.add_argument('-rho', help = 'Density', type = float)
parser.add_argument('-Lx', help = 'Panel length', type = float)
parser.add_argument('-Ly', help = 'Panel width', type = float)
parser.add_argument('-Nex', help = 'Number of streamwise elements', type = int)
parser.add_argument('-Ney', help = 'Number of spanwise elements', type = int)
parser.add_argument('-xi', help = 'Fraction of panel (0-1) to output displacement at', type = float)


# Obtain user inputs
args = parser.parse_args()

filename = args.filename
Hh = args.Hh

E = args.E
t = args.t
nu = args.nu
rho = args.rho

Lx = args.Lx
Ly = args.Ly
Nex = args.Nex
Ney = args.Ney
xi = args.xi

Nx = Nex + 1
Ny = Ney + 1

dx = Lx/Nex
dy = Ly/Ney

zfun = lambda x: Hh*t*(1 - (x - 0.5*Lx)**2/(0.5*Lx)**2)

nodes = list()
elements = list()
# Open the file and define the part
with open(filename, 'wt') as fid:
    # Put in key properties for reference.
    preamble = ('** Curved panel model written from Python. Height is defined by the relation \n'
                + '** z = (H/t)*t*(x - L/2)^2/(L/2)^2 where H/t is the ratio of rise to thickness.\n**\n'
                + '** Key parameters for this model:\n**\n'
                + '** E = %.3E\n** rho = %.3E\n** t = %.6E\n** nu = %.3f\n**\n' % (E, rho, t, nu)
                + '** Rise ratio is H/t = %.3f\n' % Hh
                + '** Length is %.3f with %i elements along the length.\n' % (Lx, Nex)
                + '** Width is %.3f with %i elements along the width.\n' % (Ly, Ney)
                + '**\n**\n')

    fid.write(preamble)
    fid.write('*Part, name=CurvedPanel\n')

    # Write all nodes.
    # Node 1 is located at (0, 0, 0).
    # Node Nx is located at (Lx, 0, 0). (x is the first index)
    # Node Nx*Ny is located at (Lx, Ly, 0) (y is the second index)
    fid.write('*Node\n')
    k = 1
    for j in range(Ny):
        for i in range(Nx):
            nodex = i*dx
            nodey = j*dy
            nodez = zfun(nodex)
            fid.write('%i, %.6E, %.6E, %.6E\n' % (k, nodex, nodey, nodez))
            nodes.append([nodex, nodey, nodez])
            k += 1

    # Write all elements.
    # Elements are numbered such that the positive normal is in the +Y direction,
    # starting with the (-X, -Y) corner.
    fid.write('*Element, type=S4R\n')
    k = 1
    for j in range(Ney):
        for i in range(Nex):
            n1 = j*Nx + i + 1
            n2 = n1 + 1
            n3 = n2 + Nx
            n4 = n3 - 1
            fid.write('%i, %i, %i, %i, %i\n' % (k, n1, n2, n3, n4))
            elements.append([n1, n2, n3, n4])
            k += 1

    # Add important nodesets (part level)
    fid.write('*Nset, nset=ends, generate\n')
    fid.write('1, %i, %i\n' % (1 + Nx*(Ny - 1), Nx))
    fid.write('%i, %i, %i\n' % (Nx, Nx*Ny, Nx))
    fid.write('*Nset, nset=sides, generate\n')
    fid.write('1, %i, 1\n' % Nx)
    fid.write('%i, %i, 1\n' % (1 + Nx*(Ny - 1), Nx*Ny))
    fid.write('*Nset, nset=disp_monitor\n')
    centerNode = round(np.floor(Ny/2)*Nx + Nx*xi)
    fid.write('%i\n' % centerNode)

    # Add shell section
    fid.write('*Elset, elset=all_elements, generate\n')
    fid.write('1, %i, 1\n' % (Ney*Nex))
    fid.write('*Shell Section, elset=all_elements, material=mat\n')
    fid.write('%.6E, 5\n' % t)

    fid.write('*End Part\n')

    # Write the assembly
    fid.write('*Assembly, name=Assembly\n')
    fid.write('*Instance, name=panel, part=CurvedPanel\n')
    fid.write('*End Instance\n')

    # Assembly nodesets and surfaces
    fid.write('*Elset, elset=_wettedSurface, internal, instance=panel, generate\n')
    fid.write('1, %i, 1\n' % (Nex*Ney))
    fid.write('*Surface, type=element, name=wetted_surface\n')
    fid.write('_wettedSurface, SPOS\n')

    fid.write('*End Assembly\n')

    # Material
    fid.write('*Material, name=mat\n')
    fid.write('*Density\n%.6E,\n' % rho)
    fid.write('*Elastic\n%.6E, %.3f\n' % (E, nu))

    # Fixed boundary conditions
    fid.write('*Boundary\n')
    fid.write('panel.ends, PINNED\n')
    fid.write('panel.sides, 2, 2\n')
    fid.write('panel.sides, 4, 4\n')
    fid.write('panel.sides, 6, 6\n')

# Also write OWC file for cosimulation
with open('owc.dat', 'wt') as fid:
    fid.write('# number of surfaces:\n1\n')
    fid.write('# number of solutions:\n0\n')
    fid.write('# node variable names:\n0\n')
    fid.write('# node variable names:\n0\n')
    fid.write('# number of element variables:\n0\n')
    fid.write('# element variable names:\n0\n')
    fid.write('# number of nodes:\n%i\n' % len(nodes))
    fid.write('# node positions:\n')

    for node in nodes:
        fid.write('%.12f %.12f %.12f\n' % (node[0], node[1], node[2]))

    fid.write('# number of faces:\n%i\n' % len(elements))
    fid.write('# Surface face definition:\n')
    for face in elements:
        fid.write('%i' % len(face))
        for ind in face[::-1]:
            fid.write(' %i' % ind)
        fid.write('\n')




