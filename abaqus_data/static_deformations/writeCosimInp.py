# -*- coding: utf-8 -*-
"""

Program to write out a curved panel to the desired shape using a given rise/thickness ratio.

"""

from numpy import sqrt as sqrt
import argparse

# Handle input arguments
parser = argparse.ArgumentParser(description = 'Write a cosimulation input file.')
parser.add_argument('-filename', help = 'File to write', required = False, default = 'cosim.inp', type = str)
parser.add_argument('-E', help = 'Modulus of elasticity', type = float)
parser.add_argument('-minf', help = 'Mach Number', type = float)
parser.add_argument('-gamma', help = 'Specific heat ratio', type = float)
parser.add_argument('-t', help = 'Thickness', type = float)
parser.add_argument('-nu', help = 'Poissons ratio', type = float)
parser.add_argument('-rho', help = 'Density', type = float)
parser.add_argument('-Lx', help = 'Panel length', type = float)
parser.add_argument('-lamb', help = 'Max ND dynamic pressure', type = float)


# Obtain user inputs
args = parser.parse_args()

filename = args.filename

E = args.E
minf = args.minf
t = args.t
nu = args.nu
rho = args.rho
L = args.Lx
lamb = args.lamb
gamma = args.gamma

# Determine freestream pressure
D = E*t*t*t/(12*(1 - nu*nu))
pinf = lamb * sqrt(minf*minf - 1)/(minf*minf) * (D/(gamma*L*L*L))

# Write cosimulation input file
with open(filename, 'wt') as fid:
    fid.write('dataFile: owc.dat\n')
    fid.write('duration: 1.0\n')
    fid.write('forceScaleFactor: 1.0\n')
    fid.write('lengthScaleFactor: 1.0\n')
    fid.write('analysisType: aeroelastic\n')
    fid.write('freestreamPressure: %.6f\n' % pinf)
    fid.write('freestreamSpeedOfSound: 340.3\n')
    fid.write('freestreamMach: %.6f\n' % minf)
    fid.write('polytropicGasConstant: %.6f\n' % gamma)
    fid.write('velocityDirection: [1, 0, 0]\n')
