# -*- coding: mbcs -*-
#
# Abaqus/Viewer Release 2016 replay file
# Internal Version: 2015_09_24-15.31.09 126547
# Run by jschonem on Sun Apr 29 15:57:49 2018
#

# from driverUtils import executeOnCaeGraphicsStartup
# executeOnCaeGraphicsStartup()
#: Executing "onCaeGraphicsStartup()" in the site directory ...
from abaqus import *
from abaqusConstants import *
session.Viewport(name='Viewport: 1', origin=(0.0, 0.0), width=254.580200195313, 
    height=199.162490844727)
session.viewports['Viewport: 1'].makeCurrent()
session.viewports['Viewport: 1'].maximize()
from viewerModules import *
from driverUtils import executeOnCaeStartup
executeOnCaeStartup()
#: Executing "onCaeStartup()" in the home directory ...
o2 = session.openOdb(name='modal.odb')
#: Model: H:/4_Projects/90504_multiphysics_development/hypersonic_stability/verification/dowell_nonlinear_flutter_curved_panels/galerkin/abaqus/frequencyConvergence/modal.odb
#: Number of Assemblies:         1
#: Number of Assembly instances: 0
#: Number of Part instances:     1
#: Number of Meshes:             1
#: Number of Element Sets:       2
#: Number of Node Sets:          4
#: Number of Steps:              1
session.viewports['Viewport: 1'].setValues(displayedObject=o2)
session.viewports['Viewport: 1'].odbDisplay.display.setValues(plotState=(
    CONTOURS_ON_DEF, ))
#: Warning: Results for the current deformed variable are not available for one or more nodes contained in the model. Deformations at such nodes are assumed to be zero.
