REM This batch file is for reference only; the wetted surface extraction routines are not publicly available.

set EXTRACT_PATH=<path to extraction routines>

REM Run Abaqus Job
call abaqus j=modal inter ask_delete=off

REM Extract system matrices from ODB
call abaqus python %EXTRACT_PATH%\modalExtract.py -odb modal.odb -surfaces WETTED_SURFACE
python %EXTRACT_PATH%\toMatlab.py WETTED_SURFACE
python %EXTRACT_PATH%\buildMatrices.py -surface WETTED_SURFACE -vhat 1 0 0
pause