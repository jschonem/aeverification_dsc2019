# -*- coding: utf-8 -*-
"""

Runs a set of analytical integrations and saves the modal time histories for later use.

"""

# This has to be run in a dedicated, separate console in order to work properly
from galerkinIntegration import eomLoop as eom

import matplotlib.pyplot as plt
import numpy as np
import time
import pickle

# This is not set up very well right now....should make it more general
# Define physical properties
E = 71000E6
rhom = 2700
nu = 0.33
h = 0.001
a = 0.5
D = E*h**3/(12*(1 - nu**2))

# Non-dimensional parameters
M = 4.0
mu = 0.1*M
tau2t = np.sqrt(rhom*h*a**4/D)
Tau = 20.0
n = 1000
tau = np.linspace(0, Tau, int(n*Tau))

saveDict = {'E': E, 'rhom': rhom, 'nu': nu, 'h': h, 'a': a, 'M': M, 'mu': mu, 'tau2t': tau2t,
            'Tau': Tau, 'tau': tau}


def compute(m, Hh, lamb):
    '''Computes a time history for given Hh and lambda and saves the key results for later examination.'''
    damp = lamb*0.5*np.sqrt(mu/(M*lamb))

    t1 = time.time()
    wLoop, yLoop = eom(m, lamb, damp, tau, Hh)
    t2 = time.time() - t1
    print('Result for lambda = %i in %.2f seconds' % (lamb, t2))

    return yLoop

def computeLambdas(m, Hh, lambdas):
    '''Computes multiple time histories for a given Hh and stores them all in a dictionary. '''
    results = dict()

    for lamb in lambdas:
        results[lamb] = compute(m, Hh, lamb)

    return results


if __name__ == '__main__':

    m = 6
    lambdas = np.array([50, 100, 150, 175, 200, 225, 250, 275, 300, 325, 350, 400, 500, 600, 700, 750, 800, 900, 1000])
    lambdas = np.array([50, 100, 146, 147, 175, 193, 194, 225, 250, 275, 300, 325, 350, 400, 500, 600, 700, 750, 800, 900, 1000])

    # Hh = [1E-3, 1.0, 2.0, 5.0]
    Hh = [1E-3, 1.0, 2.0]


    for hh in Hh:
        filestr = ('Hh%.2f' % hh).replace('.', 'pt') + '.pkl'
        saveDict['results'] = computeLambdas(m, hh, lambdas)

        with open('../python_data/lco_integration/' + filestr, 'wb') as fid:
            pickle.dump(saveDict, fid)

