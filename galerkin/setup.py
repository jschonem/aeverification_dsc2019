# -*- coding: utf-8 -*-
"""
Created on Mon Apr 23 17:25:14 2018

@author: jschonem
"""

from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
	name = 'Galerkin EOM',
	ext_modules = cythonize("galerkinIntegration.pyx"),
    include_dirs=[numpy.get_include()]
	)
