# -*- coding: utf-8 -*-
"""

Runs a set of analytical integrations and saves the modal time histories for later use.

"""

# This has to be run in a dedicated, separate console in order to work properly
# from galerkinIntegrationPython import integrate_vec as integrate
from galerkinIntegration import integrate
from galerkinIntegrationPython import prepVecMatrices as prepVecMatrices
import scipy
import matplotlib.pyplot as plt
import numpy as np
import time
import pickle

# This is not set up very well right now....should make it more general
# Define physical properties
E = 71000E6
rhom = 2700
nu = 0.33
h = 0.001
a = 0.5
D = E*h**3/(12*(1 - nu**2))

# Non-dimensional parameters
M = 10.0
mu = 0.1*M
tau2t = np.sqrt(rhom*h*a**4/D)
T = 5.0 # 1.0 seconds good for Hh = 0.0 and 1.0; 2.5 seconds for 2.0; 5.0 seconds for 5.0
n = 5000
tau = np.linspace(0, T/tau2t, int(n*T))

saveDict = {'E': E, 'rhom': rhom, 'nu': nu, 'h': h, 'a': a, 'M': M, 'mu': mu, 'tau2t': tau2t,
            'T': T, 'tau': tau}


# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()

# Obtain the static aeroelastic deflection of a panel based on the aerodynamic loading
def staticDeflection(lamb, Gammax, Ks, fs, Ipi2, oneMinusOneM, nxi = 100):
    fun = lambda q: np.linalg.norm(lamb*fs + (Ks @ q + nlForce(q, Gammax, Ipi2, oneMinusOneM)))
    sol = scipy.optimize.minimize(fun, np.zeros((m, )))
    q0 = sol.x
    xi = np.linspace(0, 1, nxi)
    # Get the physical deflection
    w = np.zeros((nxi, ))
    for i, q in enumerate(q0):
        w += q*np.sin((i + 1)*np.pi*xi)

    return q0, w



def compute(m, Hh, lamb):
    '''Computes a time history for given Hh and lambda and saves the key results for later examination.'''


    damp = lamb*0.5*np.sqrt(mu/(M*lamb))
    Gammax = 8*Hh
    Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    q0, w = staticDeflection(lamb, Gammax, Ks, fs, Ipi2, oneMinusOneM, nxi = 2)

    t1 = time.time()
    wLoop, yLoop = integrate(m, lamb, damp, tau, Hh, q0 = q0)
    t2 = time.time() - t1
    print('Result for lambda = %i in %.2f seconds' % (lamb, t2))

    return yLoop

def computeLambdas(m, Hh, lambdas):
    '''Computes multiple time histories for a given Hh and stores them all in a dictionary. '''
    results = dict()

    for lamb in lambdas:
        results[lamb] = compute(m, Hh, lamb)

    return results


if __name__ == '__main__':

    m = 6
    lambdas = np.array([50, 100, 150, 170, 210, 225, 250, 275, 300, 350, 400, 500, 600, 700, 750, 800, 900, 1000])

    # Hh = [0.00001, 1.0, 2.0, 3.5, 5.0]
    Hh = [5.0, ]


    for hh in Hh:
        filestr = ('Hh%.2f_static' % hh).replace('.', 'pt') + '.pkl'
        saveDict['results'] = computeLambdas(m, hh, lambdas)

        with open('results/' + filestr, 'wb') as fid:
            pickle.dump(saveDict, fid)

