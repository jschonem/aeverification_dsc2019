# -*- coding: utf-8 -*-
"""

Simulate a curved panel response using Galerkin's method.

Equations from E. Dowell, "Nonlinear Flutter of Curved Plates," 1969. Eq. 2.12 is the key equation
of motion.

"""

import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt

def eomLoop(z, t, n, lamb, damp, Gammax):
    ''' Loop-based implementation of Eq. 2.12 from "Nonlinear Flutter of Curved
    Plates" by E. Dowell, 1969. See Eq. 2.5 for definition of parameters.'''
    qd = z[n:]
    qdd = np.zeros(n)

    for s in range(1, n + 1):
        qs = z[s - 1]
        qds = qd[s - 1]

        oneS = 1 - (-1)**s
        ks = qs*0.5*(s*np.pi)**4  # Linear structural stiffness (uncoupled)

        kds = damp*qds  # Aerodynamic damping (uncoupled)
        fStatic = lamb*Gammax*(1 + (-1)**s)/(2*s*np.pi)  # Static aerodynamic load

        kas = 0  # Linear, coupled aerodynamic force
        knl = 0  # Nonlinear, coupled structural stiffness

        knls = qs*(s*np.pi)**2*0.5 + Gammax*oneS/(s*np.pi)  # "s" dependent stiffnes

        for m in range(1, n + 1):

            qm = z[m - 1]
            oneM = 1 - (-1)**m
            knl += (3*(m*np.pi)**2*qm*qm + 12*Gammax*qm*oneM/(m*np.pi))*knls
            if s != m:
                kas += lamb*s*m/(s*s - m*m)*(1 - (-1)**(s + m))*qm

        qdd[s - 1] = -2*(ks + kas + knl + kds + fStatic)

    return np.hstack([qd, qdd])


def tangentStiffness(q, Gammax):
    '''Loop-based tangent stiffness matrix computation.'''
    m = len(q)
    Kt = np.zeros((m, m))
    for i in range(m):
        for j in range(m):
            M = i + 1
            S = j + 1

            # First part
            Kt[i, j] = 3*(M*S*np.pi*np.pi)**2*q[i]*q[j]
            if i == j:
                for k in range(m):
                    Kt[i, j] += 1.5*(q[k]*np.pi**2*(k + 1)*S)**2

            # Second part
            Kt[i, j] += 6*Gammax*q[j]*(S*np.pi)**2*(1 - np.power(-1, M))/(M*np.pi)
            if i == j:
                for k in range(m):
                    Kt[i, j] += 6*Gammax*q[k]*(S*np.pi)**2*(1 - np.power(-1, k + 1))/((k + 1)*np.pi)

            # Third part
            Kt[i, j] += 6*Gammax*q[i]*(M*np.pi)**2*(1 - (-1)**S)/(S*np.pi)

    return Kt

def jacLoop(z, t, n, lamb, damp, Gammax):
    ''' Loop-based implementation to define the Jacobian of Eq. 2.12 from ref above.'''
    Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    Kt = tangentStiffness(z[:n], Gammax)
    Ca = damp*np.eye(n)

    return np.bmat([[np.zeros((n, n)), np.eye(n)],
                    [-2*(Ks + lamb*Ka + Kt), -2*Ca]]).T


def jacVec(z, t, m, lamb, damp, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM):
    ''' Half vectorized implementation to define the Jacobian of Eq. 2.12 from ref above.'''

    Kt = tangentStiffness(z[:m], Gammax)
    Ca = damp*np.eye(m)

    return np.bmat([[np.zeros((m, m)), np.eye(m)],
                    [-2*(Ks + Kt), -2*Ca]]).T

# @profile # Uncomment this decorator when profiling
def eomVec(z, t, n, lamb, damp, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM):
    ''' Vectorized implementation of Eq. 2.12 from "Nonlinear Flutter of Curved
    Plates" by E. Dowell, 1969. See Eq. 2.5 for definition of parameters.'''

    qd = z[n:]
    qIpi2 = z[:n]*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(z[:n]*qIpi2)*oneMinusOneM

    qdd = -2*(damp*qd + (Ks + lamb*Ka + Knl2 + Knl1a)
              @ z[:n] + lamb*fs + fnl1b.flatten())

    return np.hstack([qd, qdd])


def prepVecMatrices(m, Gammax):
    '''Prepare matrices for vectorized equation of motion implementation.'''

    # Structural stiffness matrix
    Ks = np.zeros((m, m))
    for i in range(1, m + 1):
        for j in range(1, m + 1):
            if i == j:
                Ks[i - 1, j - 1] = 0.5*(i*np.pi)**4
            Ks[i - 1, j - 1] += 12*Gammax**2/(j*i*np.pi**2)*(1 - (-1)**i)*(1 - (-1)**j)

    # Aerodynamic stiffness matrix
    Ka = np.zeros((m, m))
    for i in range(1, m + 1):
        for j in range(1, m + 1):
            if i != j:
                Ka[i - 1, j - 1] = i*j/(i*i - j*j)*(1 - (-1)**(i + j))

    # Initial static load vector
    fs = np.zeros((m, ))
    for i in range(1, m + 1):
        fs[i - 1] = Gammax*(1 + (-1)**i)/(i*2*np.pi)

    # Miscellaneous arrays for nonlinear terms
    one = np.array([np.arange(1, m + 1)])
    Ipi2 = (one*np.pi)**2
    oneMinusOneM = (1 - np.power(-1, one))/(one*np.pi)

    return Ks, Ka, fs, Ipi2, oneMinusOneM


def integrate_loop(m, lamb, damp, tau, Hh, xi = 0.75):
    '''Perform numerical integration with a set number of modes, a set dynamic pressure, and a
    specified non-dimensional time vector. Uses the loop-based procedure. Returns displacement
    at specified non-dimensional distance xi [default 0.5] normalized by panel thickness.'''

    Gammax = 8*Hh
    z0 = np.zeros((2*m, ))
    y = scipy.integrate.odeint(eomLoop, z0, tau, args = (m, lamb, damp, Gammax))

    w = np.zeros((len(tau), ))
    xi = 0.5  # Fraction of length for evaluation
    for s in range(m):
        phis = np.sin((s + 1)*np.pi*xi)
        w += y[:, s]*phis

    return w, y

def integrate_loop_jac(m, lamb, damp, tau, Hh, xi = 0.75):
    '''Perform numerical integration with a set number of modes, a set dynamic pressure, and a
    specified non-dimensional time vector. Uses the loop-based procedure. Returns displacement
    at specified non-dimensional distance xi [default 0.5] normalized by panel thickness.
    This uses the jacobian. '''

    Gammax = 8*Hh
    z0 = np.zeros((2*m, ))
    y = scipy.integrate.odeint(eomLoop, z0, tau, args = (m, lamb, damp, Gammax),
                               Dfun = jacLoop, col_deriv = True)

    w = np.zeros((len(tau), ))
    xi = 0.5  # Fraction of length for evaluation
    for s in range(m):
        phis = np.sin((s + 1)*np.pi*xi)
        w += y[:, s]*phis

    return w, y


def integrate_vec(m, lamb, damp, tau, Hh, q0 = False, qd0 = False, xi = 0.75):
    '''Perform numerical integration with a set number of modes, a set dynamic pressure, and a
    specified non-dimensional time vector. Uses the vectorized procedure. Returns displacement
    at specified non-dimensional distance xi [default 0.5] normalized by panel thickness.'''

    Gammax = 8*Hh

    # Pre-compute matrices for vectorized method
    Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    z0 = np.zeros((2*m, ))
    if type(q0) == np.ndarray:
        z0[:m] = q0
    if type(qd0) == np.ndarray:
        z0[m:] = qd0
    y = scipy.integrate.odeint(eomVec, z0, tau, args = (m, lamb, damp, Gammax, Ks,
                                                        Ka, fs, Ipi2, oneMinusOneM))

    w = np.zeros((len(tau), ))
    xi = 0.5  # Fraction of length for evaluation
    for s in range(m):
        phis = np.sin((s + 1)*np.pi*xi)
        w += y[:, s]*phis

    return w, y


def integrate_vec_jac(m, lamb, damp, tau, Hh, q0 = False, xi = 0.75):
    '''Perform numerical integration with a set number of modes, a set dynamic pressure, and a
    specified non-dimensional time vector. Uses the vectorized procedure. Returns displacement
    at specified non-dimensional distance xi [default 0.5] normalized by panel thickness.'''

    Gammax = 8*Hh

    # Pre-compute matrices for vectorized method
    Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    # jac = lambda z, t: jacVec(z, t, n, lamb, damp, Gammax, Ks, Ka)
    z0 = np.zeros((2*m, ))
    if type(q0) == np.ndarray:
        z0[:m] = q0
    y = scipy.integrate.odeint(eomVec, z0, tau, args = (m, lamb, damp, Gammax, Ks,
                                                        Ka, fs, Ipi2, oneMinusOneM),
                               Dfun = jacVec, col_deriv = True)

    w = np.zeros((len(tau), ))
    xi = 0.5  # Fraction of length for evaluation
    for s in range(m):
        phis = np.sin((s + 1)*np.pi*xi)
        w += y[:, s]*phis

    return w, y


if __name__ == '__main__':

    import time

    # Define physical properties
    E = 71000E6
    rhom = 2700
    nu = 0.33
    h = 0.001
    a = 0.5
    D = E*h**3/(12*(1 - nu**2))

    # Non-dimensional parameters
    Hh = 1.0
    M = 10.0
    mu = 0.1*M
    tau2t = np.sqrt(rhom*h*a**4/D)
    T = 10.0
    n = 1000
    tau = np.linspace(0, T, int(n*T))

    # Number of modes, dynamic pressure, and damping coefficient
    m = 6
    lamb = 350
    damp = lamb*0.5*np.sqrt(mu/(M*lamb))

    # Obtain response (loop method)
#    t1 = time.time()
#    wLoop, yLoop = integrate_loop(m, lamb, damp, tau, Hh)
#    tloop = time.time() - t1
#    plt.plot(tau*tau2t, wLoop)
#    print('Loop-based solution: %.1fs physical time in %.2fs simulation time' % (T, tloop))

    # Obtain response (loop method with Jacobian)
    t1 = time.time()
    wVecJac, yVecJac = integrate_vec_jac(m, lamb, damp, tau, Hh)
    tloop = time.time() - t1
    plt.plot(tau, wVecJac)
    print('Vector-based solution w/Jacobian: %.1fs physical time in %.2fs simulation time' % (T, tloop))

    # Vectorized method
    t1 = time.time()
    wVec, yVec = integrate_vec(m, lamb, damp, tau, Hh)
    tVec = time.time() - t1
    plt.plot(tau, wVec, '--')
    print('Vectorized solution: %.1fs physical time in %.2fs simulation time' % (T, tVec))

