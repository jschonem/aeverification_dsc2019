# -*- coding: utf-8 -*-
"""

Simulate a curved panel response using Galerkin's method.

Equations from E. Dowell, "Nonlinear Flutter of Curved Plates," 1969. Eq. 2.12 is the key equation
of motion.

"""

import numpy as np
import scipy.integrate
import cython
import matplotlib.pyplot as plt

# Now bring in the C library math functions
from libc.math cimport sqrt, pow
cimport cython # To turn off division error checking
cimport numpy as np
ctypedef np.float64_t float64

# Define physical properties
cdef float64 E = 71000E6
cdef float64 rhom = 2700
cdef float64 nu = 0.33
cdef float64 h = 0.001
cdef float64 a = 0.5
cdef float64 D = E*h*h*h/(12*(1 - nu*nu))

# Non-dimensional parameters
cdef float64 Hh = 1.5
cdef float64 M = 10.0
cdef float64 mu = 0.1*M
# cdef float64 Gammax = 8.0*Hh
cdef float64 tau2t = sqrt(rhom*h*a*a*a*a/D)

# Constants
cdef float64 PI = np.pi
cdef float64 ZERO = 0.0
cdef float64 HALF = 0.5
cdef float64 ONE = 1.0
cdef float64 TWO = 2.0
cdef float64 THREE = 3.0
cdef float64 FOUR = 4.0
cdef float64 TWELVE = 12

# cdef int ONEi = 1

cdef float64 negativeOnePow(int m) except ?-2:
    '''Returns negative one to the mth power.'''
    if m % 2: # If m is odd
        return -1.0
    else:
        return 1.0

@cython.cdivision(True)
cdef float64 linearStiffnessIJ(int i, int j, float64 s, float64 m, float64 Gammax) except*:
    '''Compute (i, j)th entry of the linear stiffness matrix; i and j are supplied as 0-indexed.'''

    cdef float64 ks = 12*Gammax**2/(s*m*PI*PI)*(1 - negativeOnePow(j + 1))*(1 - negativeOnePow(i + 1))
    if (i == j):
        ks += 0.5*(s*s*s*s*PI*PI*PI*PI)
    return ks

@cython.cdivision(True)
cdef float64 nlStiffnessIJ(int i, int j, float64 s, float64 m, float64 Gammax, float64 qs, float64 qm) except*:
    '''Compute (i, j)th contribution to the nonlinear stiffness vector.'''

    cdef float64 knls = qs*(s*s*PI*PI)*0.5 # "s" dependent stiffnes
    cdef float64 oneS = 1.0 - negativeOnePow(i + 1)
    cdef float64 oneM = 1.0 - negativeOnePow(j + 1)
    return 3.0*(m*m*PI*PI)*qm*qm*(knls + Gammax*oneS/(s*PI))+ 12.0*Gammax*qm*oneM/(m*PI)*knls

@cython.cdivision(True)
cdef float64 aeroStiffnessIJ(int i, int j, float64 s, float64 m, float64 lamb) except*:
    '''Compute IJ-th entry of the aerodynamic stiffness matrix'''
    if i == j:
        return 0.0

    return lamb*s*m/(s*s - m*m)*(1 - negativeOnePow(i + j + 2))

@cython.cdivision(True)
cdef float64 staticForceI(float64 s, int i, float64 lamb, float64 Gammax) except*:
    '''Compute ith entry of the static force vector.'''

    return lamb*Gammax*(ONE + negativeOnePow(i + 1))/(TWO*s*PI)  # Static aerodynamic load

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef float64 tangentStiffnessIJ(int i, int j, float64 s, float64 m, np.ndarray[float64, ndim=1] q, int n, float64 Gammax) except*:
    '''Loop-based tangent stiffness matrix computation.'''

    cdef int k
    cdef float64 kt, kf, qi, qj, qk

    # First part
    qi = q[i]
    qj = q[j]
    kt = 3*(m*s*PI*PI)*(m*s*PI*PI)*qi*qj
    if i == j:
        for k in range(n):
            qk = q[k]
            kf = float(k) + 1
            kt += 1.5*(qk*PI*PI*kf*s)*(qk*PI*PI*kf*s)

    # Second part
    kt += 6*Gammax*qi*(s*s*PI*PI)*(1 - negativeOnePow(j + 1))/(m*PI)
    if i == j:
        for k in range(n):
            kf = float(k) + 1
            qk = q[k]
            kt += 6*Gammax*qk*(s*s*PI*PI)*(1 - negativeOnePow(k + 1))/(kf*PI)

    # Third part
    kt += 6*Gammax*qj*(m*m*PI*PI)*(1 - negativeOnePow(i + 1))/(s*PI)

    return kt

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def jacobian(np.ndarray[float64, ndim=1] z, float64 t, int n, float64 lamb, float64 damp, float64 Gammax,
             np.ndarray[float64, ndim=1] zout, np.ndarray[float64, ndim=2] Jout):
    ''' Loop-based implementation to define the Jacobian of Eq. 2.12 from ref above.'''

    # Only need to modify stiffness derivative
    cdef int i, j
    cdef float64 s, m
    for i in range(n):
        s = float(i) + 1
        for j in range(n):
            m = float(j) + 1
            Jout[j, i + n] = (linearStiffnessIJ(i, j, s, m, Gammax) +
                              aeroStiffnessIJ(i, j, s, m, lamb) +
                              tangentStiffnessIJ(i, j, s, m, z, n, Gammax))

    return Jout


def prepJacobian(float64 damp, int n):
    '''Define the fixed derivatives of acceleration w.r.t. velocity'''
    cdef np.ndarray[float64, ndim=2] Jout = np.zeros((2*n, 2*n))

    for i in range(n):
        Jout[n, i + n] = 1.0
        Jout[n + i, n + i] = damp

    return Jout

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def eom(np.ndarray[float64, ndim=1] z, float64 t, int n, float64 lamb, float64 damp,
                float64 Gammax, np.ndarray[float64, ndim = 1] zout, np.ndarray[float64, ndim=2] Jout):
    ''' Loop-based implementation of Eq. 2.12 from "Nonlinear Flutter of Curved
    Plates" by E. Dowell, 1969. See Eq. 2.5 for definition of parameters.'''

    cdef int i, j
    cdef float64 s, m
    cdef float64 qs, qds, oneS, ks, kds, fStatic
    cdef float64 kas, knl, knls, qm, oneM

    for i in range(n):
        qs = z[i]
        qds = z[i + n]

        s = float(i) + 1.0
        ks = ZERO
        kds = damp*qds  # Aerodynamic damping (uncoupled)
        fStatic = staticForceI(s, i, lamb, Gammax)
        kas = ZERO  # Linear, coupled aerodynamic force
        knl = ZERO  # Nonlinear, coupled structural stiffness

        for j in range(n):
            m = float(j) + 1.0
            qm = z[j]

            ks += linearStiffnessIJ(i, j, s, m, Gammax)*qm
            knl += nlStiffnessIJ(i, j, s, m, Gammax, qs, qm)
            kas += aeroStiffnessIJ(i, j, s, m, lamb)*qm

        zout[i + n] = -TWO*(ks + kas + knl + kds + fStatic)
        zout[i] = qds

    return zout


def eomLoop(int m, float64 lamb, float64 damp, np.ndarray[float64, ndim=1] tau,
                       float64 Hh, float64 xi = 0.75):

    cdef int s
    cdef float64 Gammax = 8.0*Hh
    cdef np.ndarray[float64, ndim=1] z0 = np.zeros((2*m, ))
    cdef np.ndarray[float64, ndim=1] zout = np.zeros(2*m)
    cdef np.ndarray[float64, ndim=2] Jout = prepJacobian(damp, m)

    y = scipy.integrate.odeint(eom, z0, tau, args = (m, lamb, damp, Gammax, zout, Jout),
                                 Dfun = jacobian, col_deriv = True)

    cdef np.ndarray[float64, ndim = 1] w = np.zeros((len(tau), ))
    # cdef float64 xi = 0.5  # Fraction of length for evaluation
    for s in range(0, m):
        w += y[:, s]*np.sin((s + 1)*np.pi*xi)

    return w, y

# Other, convenience functions
def getTangentStiffness(q, Gammax):
    '''Obtain the full tangent stiffness matrix at the given deformation.'''
    cdef int n = len(q)
    cdef np.ndarray[float64, ndim=2] Kt = np.zeros((n, n))
    cdef int i, j
    cdef float64 s, m
    cdef np.ndarray[float64, ndim = 1] qc = np.zeros(q.shape)
    qc[:] = q
    for i in range(n):
        s = float(i) + 1
        for j in range(n):
            m = float(j) + 1
            Kt[i, j] = tangentStiffnessIJ(i, j, s, m, qc, n, float(Gammax))

    return Kt


if __name__ == '__main__': # This doesn't do anything when it's compiled through Spyder

    import time

    # Define physical properties
    E = 71000E6
    rhom = 2700
    nu = 0.33
    h = 0.001
    a = 0.5
    D = E*h**3/(12*(1 - nu**2))

    # Non-dimensional parameters
    Hh = 1.5
    M = 10.0
    mu = 0.1*M
    tau2t = np.sqrt(rhom*h*a**4/D)
    T = 1.0
    n = 1000
    tau = np.linspace(0, T/tau2t, int(n*T))

    # Number of modes, dynamic pressure, and damping coefficient
    m = 6
    lamb = 500
    damp = lamb*0.5*np.sqrt(mu/(M*lamb))

    # Obtain response (loop method)
    t1 = time.time()
    wLoop, yLoop = eomLoop(m, lamb, damp, tau, Hh)
    tloop = time.time() - t1
    plt.plot(tau*tau2t, wLoop)
    print('Loop-based solution: %.1fs physical time in %.2fs simulation time' % (T, tloop))
