# -*- coding: utf-8 -*-
"""

Evaluates late-time dynamic response computed using Galerkin/Python routines. Helps to manually
classify the type of response (stable, LCO, or chaotic) that results from integration conditions.

"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy.io

#sys.path.append('../integrations')

#import runIntegration


# Hh = [0.0, 1.0, 2.0, 5.0]
Hh = 0.0

stabCodes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 0.0)
# stabCodes = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 1.0)
# stabCodes = [0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1] # (H/h = 2.0)
# stabCodes = [0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 5.0)
stabTypes = ['stable', 'periodic LCO', 'chaotic/non-periodic']
fileCode = 'hh%.0fpt%.0f' % (round(Hh), 10*(Hh - round(Hh)))
resultsPath = '../abaqus_data/direct_integration/explicitVDLOAD/%s/results' % fileCode

filestr = lambda lamb: ('explicit_%i' % lamb)

# Parameters for non-dimensionalizing
E = 7.1E10
rho = 2.7E3
h = 1E-3
nu = 0.33
a = 0.5
D = E*h**3/(12*(1 - nu**2))
t2tau = 1/np.sqrt(rho*h*a**4/D)


dtau = 2 # Last portion of tau to look at

# Read files
files = os.listdir(resultsPath)
lambs = [int(s.split('_')[-1]) for s in files]
lambs.sort()
key = 'Node PANEL.223_U3'
fig, ax = plt.subplots(len(files), 1)
fig.set_figheight(40)
fig.set_figwidth(5)
k = 0

for lamb in lambs:
    with open(os.path.join(resultsPath, filestr(lamb)), 'rb') as fid:
        data = pickle.load(fid, encoding = 'latin1')

    tau = data[key][:, 0]*t2tau
    w = data[key][:, 1]/h
    Tau = tau[-1]

    keepinds = tau > (Tau - dtau)

    stabCode = stabCodes[k]
    ax[k].set_title('$\mathbf{\lambda=%.0f}$, i = %i, Stability = %s' % (lamb,
                    k, stabTypes[stabCode]), fontweight = 'bold')
    # xlim = ax[k].get_xlim()
    ax[k].set_xlim([Tau - dtau, Tau])
    ax[k].set_xlabel('')

    ax[k].plot(tau[keepinds], w[keepinds])


    imax = np.argmax(np.abs(w[keepinds]))
    wmax = np.abs(w[imax])

    ax[k].plot(ax[k].get_xlim(), [w[keepinds][imax], w[keepinds][imax]], 'k--')

    k += 1
