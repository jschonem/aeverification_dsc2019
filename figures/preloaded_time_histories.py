# -*- coding: utf-8 -*-
"""

Examine stability behavior for statically loaded panel by plotting some time histories of
preloaded and unloaded panels.

"""

import numpy as np
import matplotlib.pyplot as plt
import sys
import scipy


# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness


# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()

# Obtain the static aeroelastic deflection of a panel based on the aerodynamic loading
def staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 100):
    fun = lambda q: np.linalg.norm(lamb*fs +
                                   ((lamb*Ka + Ks) @ q + nlForce(q, Gammax, Ipi2, oneMinusOneM)))
    sol = scipy.optimize.minimize(fun, np.zeros((m, )))
    q0 = sol.x
    xi = np.linspace(0, 1, nxi)
    # Get the physical deflection
    w = np.zeros((nxi, ))
    for i, q in enumerate(q0):
        w += q*np.sin((i + 1)*np.pi*xi)

    return q0, w

# Obtain nolinear portion of the tangent stiffness matrix at a given deflection
def tangentStiffness(q, Gammax):
    m = len(q)
    Kt = np.zeros((m, m))
    for i in range(m):
        for j in range(m):
            S = i + 1
            M = j + 1

            # First part
            Kt[i, j] = 3*(M*S*np.pi*np.pi)**2*q[i]*q[j]
            if i == j:
                for k in range(m):
                    Kt[i, j] += 1.5*(q[k]*np.pi**2*(k + 1)*S)**2

            # Second part
            Kt[i, j] += 6*Gammax*q[i]*(S*np.pi)**2*(1 - np.power(-1, M))/(M*np.pi)
            if i == j:
                for k in range(m):
                    Kt[i, j] += 6*Gammax*q[k]*(S*np.pi)**2*(1 - np.power(-1, k + 1))/((k + 1)*np.pi)

            # Third part
            Kt[i, j] += 6*Gammax*q[j]*(M*np.pi)**2*(1 - (-1)**S)/(S*np.pi)

    return Kt


# Model setup
muM = 0.1
m = 6
Cag = np.eye(m)
Zg = np.zeros((m, m))
Ig = np.eye(m)

Hh = 5.0
Gammax = 8*Hh
Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)

lambdaVec = [146, 147, 193, 194] # Dowell has instability at lambda = 144, I have isntability at
 # lambda = 195 when releasing from equilibrium

T = 10.0
nt = 1000
tau = np.linspace(0, T, int(T*nt))
qd0 = np.zeros((m, ))
qd0[0] = 0.1

wBase = np.zeros((len(tau), 4))
wStatic = np.zeros((len(tau), 4))

for ind, lamb in enumerate(lambdaVec):

    # Static solution
    q0, w = staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 2)
    Kt = tangentStiffness(q0, Gammax)

    gdamp = np.sqrt(muM*lamb)

    A = np.bmat([[Zg, Ig], [-2*lamb*Ka - 2*(Ks + Kt), -gdamp*Cag]])
    v = np.linalg.eigvals(A)[::2]

    omega = np.min(np.imag(v))

    # Dynamic solution from base state
    damp = 0.5*np.sqrt(muM*lamb)
    wBase[:, ind], yBase = integrate_vec(m, lamb, damp, tau, Hh)

    # Dynamic solution from preloaded state

    wStatic[:, ind], yStatic = integrate_vec(m, lamb, damp, tau, Hh, q0 = q0, qd0 = qd0)

#%% Plot results

# Some nicer colors
blue = lambda alpha: [0.1, 0.2, 0.7, alpha]
red = lambda alpha: [0.8, 0.25, 0.2, alpha]

fig, ax = plt.subplots(2, 2)
fig.set_figheight(6)
fig.set_figwidth(12)
ax = ax.flatten()

for ind in range(4):
    ax[ind].plot(tau, wStatic[:, ind], color = blue(1), label = 'Released from Static Eq.')
    ax[ind].plot(tau, wBase[:, ind], color = red(1), label = 'Released from Base State')
    ax[ind].set_title('$\mathbf{\lambda = %.0f}$' % lambdaVec[ind])

    ax[ind].set_xlim([tau[0], tau[-1]])
    ax[ind].set_ylim([-12, 2])

# Other formats
ax[0].legend(fontsize = 12)
ax[0].set_ylabel('Displacement/Thickness', fontsize = 12, fontweight = 'bold')
ax[2].set_ylabel('Displacement/Thickness', fontsize = 12, fontweight = 'bold')
ax[2].set_xlabel('Non-Dimensional Time', fontsize = 12, fontweight = 'bold')
ax[3].set_xlabel('Non-Dimensional Time', fontsize = 12, fontweight = 'bold')

ax[0].set_xticklabels([])
ax[1].set_xticklabels([])
#ax[1].set_yticklabels([])
#ax[3].set_yticklabels([])


fig.savefig('images/preloaded_time_histories.png', bbox_inches = 'tight', transparent = True)