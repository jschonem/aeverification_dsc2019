# -*- coding: utf-8 -*-
"""

Makes plots relating to the flat panel dynamics for the Galerkin and the ROM.

"""

import helper
helper.addpaths()
import os
import numpy as np
import sys
import matplotlib.pyplot as plt
import scipy
import scipy.io

# Add custom modules to path
hyperStablePath = 'U:\\jschonem\\4_Projects\\90504_multiphysics_development\\hypersonic_stability\\python\\hyperStable'
if hyperStablePath not in sys.path:
    sys.path.append(hyperStablePath)

# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices

# File management
wettedSurface = 'WETTED_SURFACE.mat'
wettedSurfaceArrays = 'WETTED_SURFACE_arrays.mat'

# Define physical properties
E = 71000E6
rho = 2700
nu = 0.33
t = 0.001
Lx = 0.5
D = E*t**3/(12*(1 - nu**2))

# Non-dimensional parameters
Hh = 5.0
M = 10.0
mu = 0.1*M
tau2t = np.sqrt(rho*t*Lx**4/D)
T = 1.0
n = 1000
tau = np.linspace(0, T/tau2t, int(n*T))

# Compute freq/damping for Galerkin model
def freqDampGal(lamb, muM, nfreqs, Ksg, Kag, Cag, Zg, Ig):

    # Compute damping value
    gdamp = np.sqrt(muM*lamb)

    A = np.bmat([[Zg, Ig], [-2*lamb*Kag - 2*Ksg, -gdamp*Cag]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    return np.abs(np.imag(v[inds[:nfreqs]])), np.real(v[inds[:nfreqs]])

# Other flow parameters
gamma = 1.4

def setpInf(lamb, mInf, gamma, a, D):
    """
    Set the required static pressure to obtain a desired lambda given the freestream Mach number,
    specific heat ratio gamma, panel length a, and panel rigidity D.
    """

    return lamb * np.sqrt(mInf*mInf - 1)/(mInf*mInf) * (D/(gamma*a*a*a))


def setaInf(muM, pInf, mInf, a, gamma, rhoM, t):
    """
    Set the freestream speed of sound to obtain a desired rati of mu/M.
    """

    return np.sqrt(1/muM * (pInf/mInf) * (a*gamma/(rhoM*t)))

# Function to compute frequency/damping for the ROM
def freqDampRom(lamb, muM, nfreqs, Lambda, Ka, Ca, Z, I):
    # Obtain equivalent pressure and speed of sound
    pInf = setpInf(lamb, M, gamma, Lx, D)
    aInf = setaInf(muM, pInf, M, Lx, gamma, rho, t)
    U = M*aInf

    A = np.bmat([[Z, I], [pInf*Ka - Lambda, pInf/U*Ca]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    return np.abs(np.imag(v[inds[:nfreqs]])), np.real(v[inds[:nfreqs]])

def flutterBoundary(freqDampFun, muMVec, Ks, Ka, Ca, Z, I):
    '''Obtain flutter boundary in terms of mu/M values for given system matrices.'''
    boundary = np.zeros((len(muMVec), ))
    nfreqs = 2
    nl = 100
    lambVec = np.linspace(10, 1000, nl)

    for mind, muM in enumerate(muMVec):
        damping = np.zeros((nl, nfreqs))
        for ind, lamb in enumerate(lambVec):
            freq, damping[ind, :] = freqDampFun(lamb, muM, nfreqs, Ks, Ka, Ca, Z, I)

        maxDamping = np.max(damping, axis = 1)
        signChange = np.where(np.diff(np.sign(maxDamping)))[0][0]
        # print('ind = %i; muM = %.2f; signChange = %i' % (ind, muM, signChange))
        boundary[mind] = np.interp(0, maxDamping[signChange:(signChange + 2)],
                         lambVec[signChange:(signChange + 2)])


    return boundary


Hhvec = [0.0, 1.0, 2.0, 5.0]
#Hhvec = [0.0, ]
# First, look at just the frequency and damping evolution with dynamic pressure
nmuM = 20
gA = np.linspace(0.1, 1, nmuM)

muMVec = np.linspace(0.01, 0.5, nmuM)
m = 6 # Galerkin modes
nm = 20 # ROM modes
nFreqs = 2 # Number of frequencies to store

# Set up figure
fig = plt.figure(figsize = (12, 6))
plt.xlim([muMVec[0], muMVec[-1]])
plt.ylim([100, 600])

# omega0 = np.pi**2*np.sqrt(D/(rho*t*Lx**4))
# orientations = [-1, 1, 1, 1]

# Some nicer colors
galerkinColor = (0.2, 0.2, 0.6, 1.0)
abaqusEdgeColor = (0.6, 0.2, 0.2, 1.0)
abaqusFaceColor = (0.6, 0.2, 0.2, 0.75)
refFaceColor = (0.0, 0.0, 0.0, 0.75)
refEdgeColor = (0.0, 0.0, 0.0, 1.0)


for hind, Hh in enumerate(Hhvec):

    romDir = '../' + ('abaqus_data/undeformed_dynamics/Hh_%.2f' % Hh).replace('.', 'pt')

    # Load up Abaqus files
    ws = scipy.io.loadmat(os.path.join(romDir, wettedSurface))
    wsa = scipy.io.loadmat(os.path.join(romDir, wettedSurfaceArrays))

    # Galerkin model
    Ksg, Kag, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Hh*8)
    Cag = np.eye(m)
    Zg = np.zeros((m, m))
    Ig = np.eye(m)

    # ROM parameters
    nm = 20
    phi = ws['phi'][:, :nm]
    omega = (ws['fn'].flatten()[:nm]*2*np.pi)**2
    Lambda = np.diag(omega)
    Z = np.zeros((nm, nm))
    I = np.eye(nm)

    stiffnessCoeff = gamma*M**2/np.sqrt(M**2 - 1)
    dampingCoeff = stiffnessCoeff*((M**2 - 2)/(M**2 - 1))

    Ka = stiffnessCoeff*phi.T @ wsa['D'] @ wsa['A'] @ phi # Need to multiply by pInf
    Ca = dampingCoeff*phi.T @ wsa['D'] @ wsa['B'] @ phi # Need to multiply by pInf/U

    boundaryRom = flutterBoundary(freqDampRom, muMVec, Lambda, Ka, Ca, Z, I)
    boundaryGal = flutterBoundary(freqDampGal, muMVec, Ksg, Kag, Cag, Zg, Ig)

    if hind == 0:
        plt.plot(muMVec, boundaryGal, linewidth = 3, color = galerkinColor)[0].set_label(
                 '%i-Mode Galerkin' % m)
    else:
        plt.plot(muMVec, boundaryGal, linewidth = 3, color = galerkinColor)

    if hind == 0:
        plt.plot(muMVec, boundaryRom, '^', linewidth = 1.5, markerfacecolor = abaqusFaceColor,
                 markeredgecolor = abaqusEdgeColor)[0].set_label(
                 'Abaqus/ROM')
    else:
        plt.plot(muMVec, boundaryRom, '^', linewidth = 1.5, markerfacecolor = abaqusFaceColor,
                 markeredgecolor = abaqusEdgeColor)

    textX = muMVec[-1]
    textY = boundaryRom[-1] + 10

    # Figure out slope angle at the end of each plot
    indices = [int(nmuM*.75), -1]
    Y = boundaryRom[indices]
    X = muMVec[indices]

    xy_pixels = plt.gca().transData.transform(np.vstack([X, Y]).T)
    xpix, ypix = xy_pixels.T
    rotate = np.arctan(np.diff(ypix)/np.diff(xpix))[0]/np.pi*180

    plt.text(textX, textY, 'H/h = %.1f' % Hh, fontsize = 14, horizontalAlignment = 'right',
             rotation = rotate)
             #bbox=dict(facecolor='red', alpha=0.5))


# Other formatting
plt.xlabel('Mass/Mach Ratio $\mathbf{\mu/M_{\infty}}$', fontsize = 14, fontweight = 'bold')
plt.ylabel('Flutter Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
plt.legend(fontsize = 13, loc = 2)

helper.save(fig, 'images/large/fig04_unloaded_panelDynamics.svg', 8, 16)
# helper.save(fig, 'images/small/fig04_unloaded_panelDynamics.png', 5, 12)


