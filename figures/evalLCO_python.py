# -*- coding: utf-8 -*-
"""

Evaluates late-time dynamic response computed using Galerkin/Python routines. Helps to manually
classify the type of response (stable, LCO, or chaotic) that results from integration conditions.

"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy.io

#sys.path.append('../integrations')

#import runIntegration


# Hh = [0.0, 1.0, 2.0, 5.0]
Hh = [5.0, ]

# stabCodes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 0.0)
# stabCodes = [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 1.0)
# stabCodes = [0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1] # (H/h = 2.0)
stabCodes = [0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1] # (H/h = 5.0)
stabTypes = ['stable', 'periodic LCO', 'chaotic/non-periodic']

resultsPath = '../python_data/lco_integration'
filestr = lambda hh: ('Hh%.2f' % hh).replace('.', 'pt') + '.pkl'
dtau = 5 # Last portion of tau to look at
xi = 0.75
m = 6
phi = np.zeros((m, ))
for i in range(m):
    phi[i] = np.sin((i + 1)*np.pi*xi)

for ind, hh in enumerate(Hh):

    with open(os.path.join(resultsPath, filestr(hh)), 'rb') as fid:
        data = pickle.load(fid)
        results = data['results']

        fig, ax = plt.subplots(len(results), 1)
        fig.set_figheight(40)
        fig.set_figwidth(5)
        k = 0

        tau = data['tau']
        keepinds = tau > ((data['Tau']) - dtau)
        taukeep = tau[keepinds]

        for lamb, q in results.items():
            m = int(q.shape[1]/2)

            q = q[keepinds, :m]

            w = phi @ q.T

            ax[k].plot(taukeep, w)
            stabCode = stabCodes[k]
            ax[k].set_title('$\mathbf{\lambda=%.0f}$, i = %i, Stability = %s' % (lamb,
                            k, stabTypes[stabCode]), fontweight = 'bold')
            # xlim = ax[k].get_xlim()
            ax[k].set_xlim([data['Tau'] - dtau, data['Tau']])
            ax[k].set_xlabel('')


            wkeep = w
            imax = np.argmax(np.abs(wkeep))
            wmax = np.abs(wkeep[imax])

            ax[k].plot(ax[k].get_xlim(), [wkeep[imax], wkeep[imax]], 'k--')


            k += 1

#            mu = np.mean(w, axis = 0)
#            std = np.std(w - mu, axis = 0)
#            W = np.max(np.abs(w), axis = 1)
#            rms = np.sqrt(mu*mu + std*std)
#            adjust = np.abs(mu) + np.sqrt(2)*std
            # Wmax.append(np.max(adjust))
            # Wmax.append(np.max(W))
            # Wmax.append(np.sqrt(2)*np.max(std))

#        ax[ind].plot(list(data['results'].keys()), Wmax, 'o-', label = 'Present',
#              color = (0.2, 0.2, 0.6, 1.0),
#              markerfacecolor = (0.2, 0.2, 0.6, 0.75), markeredgecolor = (0.2, 0.2, 0.6, 1.0))
#
#        # Add digitized data
#        dig = np.array(digiDat[labels[ind]])
#        a, b = pix2plot(dig[:, 0], dig[:, 1])
#        marker = ax[ind].plot(a, b, 'ks', markeredgewidth = 2.0, label = 'Ref [5]',
#                   markerfacecolor = (0.0, 0.0, 0.0, 0.45), markeredgecolor = (0.0, 0.0, 0.0, 1.0))
##        for m in marker:
##            m.set_markerfacecolor((0.0, 0.0, 0.0, 0.5))
##            m.set_markeredgecolor((0.0, 0.0, 0.0, 1.0))
#        ax[ind].set_xlim([50, 1000])
#        ax[ind].set_title('H/h = %.1f' % hh, fontsize = 14, fontweight = 'bold')

## Add format items
#ax[0].legend(fontsize = 13)
#ax[0].set_ylabel('$\mathbf{W_{max}}$', fontsize = 14)
#ax[2].set_ylabel('$\mathbf{W_{max}}$', fontsize = 14)
#
#ax[2].set_xlabel('$\mathbf{\lambda}$', fontsize = 14)
#ax[3].set_xlabel('$\mathbf{\lambda}$', fontsize = 14)
#
#
#fig.savefig('images/LCO_compare_galerkin.png', bbox_inches = 'tight', transparent = True)