# -*- coding: utf-8 -*-
"""

Makes plots relating to the flat panel dynamics for the Galerkin and the ROM.

"""

import helper
helper.addpaths()

import os
import pickle
import numpy as np
import sys
import matplotlib.pyplot as plt

import scipy
import scipy.io
# TODO: The ROM has to write out and save results in separate files, because it can't be distributed
# along with the other files.

# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness

# File loading
matfile = 'figure3.mat'
matdat = scipy.io.loadmat(os.path.join('digitized', matfile))

# Digitization of data
X1Y0 = np.array(matdat['X1Y0'][0], dtype = float)
X0Y1 = np.array(matdat['X0Y1'][0], dtype = float)
X0 = X0Y1[0]
Y0 = X1Y0[1]
DX = X1Y0[0] - X0
DY = X0Y1[1] - Y0

x = np.array(matdat['xrange'][0], dtype = float); dx = x[1] - x[0]
y = np.array(matdat['yrange'][0], dtype = float); dy = y[1] - y[0]

pix2plot = lambda X, Y: (dx*(X - X0)/DX + x[0], dy*(Y - Y0)/DY + y[0])

dowell_withStaticHh, dowell_withStaticLambda = pix2plot(matdat['withstatic'][:, 0], matdat['withstatic'][:, 1])
dowell_noStaticHh, dowell_noStaticLambda = pix2plot(matdat['nostatic'][:, 0], matdat['nostatic'][:, 1])

#dowell_withStaticHh = np.array([0, 1, 2, 3.5, 5])
#dowell_withStaticLambda = np.array([350, 280, 180, 170, 150])
#dowell_noStaticHh = np.array([0, 1, 2, 3.5, 5])
#dowell_noStaticLambda = np.array([350, 180, 110, 410, 500])

# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()

# Obtain the static aeroelastic deflection of a panel based on the aerodynamic loading
def staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 100):
    fun = lambda q: np.linalg.norm(lamb*fs +
                                   ((lamb*Ka + Ks) @ q + nlForce(q, Gammax, Ipi2, oneMinusOneM)))
    sol = scipy.optimize.minimize(fun, np.zeros((m, )))
    q0 = sol.x
    xi = np.linspace(0, 1, nxi)
    # Get the physical deflection
    w = np.zeros((nxi, ))
    for i, q in enumerate(q0):
        w += q*np.sin((i + 1)*np.pi*xi)

    return q0, w

# Obtain nolinear portion of the tangent stiffness matrix at a given deflection
def tangentStiffness(q, Gammax):
    m = len(q)
    Kt = np.zeros((m, m))
    for i in range(m):
        for j in range(m):
            S = i + 1
            M = j + 1

            # First part
            Kt[i, j] = 3*(M*S*np.pi*np.pi)**2*q[i]*q[j]
            if i == j:
                for k in range(m):
                    Kt[i, j] += 1.5*(q[k]*np.pi**2*(k + 1)*S)**2

            # Second part
            Kt[i, j] += 6*Gammax*q[i]*(S*np.pi)**2*(1 - np.power(-1, M))/(M*np.pi)
            if i == j:
                for k in range(m):
                    Kt[i, j] += 6*Gammax*q[k]*(S*np.pi)**2*(1 - np.power(-1, k + 1))/((k + 1)*np.pi)

            # Third part
            Kt[i, j] += 6*Gammax*q[j]*(M*np.pi)**2*(1 - (-1)**S)/(S*np.pi)

    return Kt


# Compute freq/damping for Galerkin model
def freqDampGal(lamb, muM, nfreqs, Ksg, Kag, Cag, Zg, Ig):

    # Compute damping value
    gdamp = np.sqrt(muM*lamb)

    A = np.bmat([[Zg, Ig], [-2*lamb*Kag - 2*Ksg, -gdamp*Cag]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    return np.abs(np.imag(v[inds[:nfreqs]])), np.real(v[inds[:nfreqs]])


def criticalBoundariesStatic(lambdaVec, muM, Ks, Ka, Ca, Z, I, Gammax, fs, Ipi2, oneMinusOneM):
    '''Obtain flutter and divergence boundary (if it exists in the supplied range)
    in terms of mu/M values for given system matrices, considering the effects of static preload.'''

    n = len(lambdaVec)
    nfreqs = 4

    damping = np.zeros((n, nfreqs))
    freq = np.zeros((n, nfreqs))

    for ind, lamb in enumerate(lambdaVec):
        q0, w = staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 2)
        Kt = tangentStiffness(q0, Gammax)
        freq[ind, :], damping[ind, :] = freqDampGal(lamb, muM, nfreqs, Ks + Kt, Ka, Ca, Z, I)

    # Obtain flutter boundary
    maxDamping = np.max(damping, axis = 1)
    signChange = np.where(np.diff(np.sign(maxDamping)))[0]
    if len(signChange) == 0:
        flutter = None
    else:
        signChange = signChange[0]
        flutter = np.interp(0, maxDamping[signChange:(signChange + 2)],
                                   lambdaVec[signChange:(signChange + 2)])

    # Obtain divergence boundary
    minFreqs = np.min(freq, axis = 1)
    divIndex = np.where(minFreqs == 0)[0]
    if len(divIndex) == 0:
        divergence = None
    else:
        divIndex = divIndex[0]
        divergence = lambdaVec[divIndex] # This could be more accurate

    return flutter, divergence, freq, damping

def flutterBoundary(lambdaVec, muM, Ks, Ka, Ca, Z, I):
    '''Obtain flutter boundary in terms of mu/M values for given system matrices.'''
    n = len(lambdaVec)
    boundary = np.zeros((n, ))
    nfreqs = 2

    damping = np.zeros((n, nfreqs))
    for ind, lamb in enumerate(lambdaVec):
        freq, damping[ind, :] = freqDampGal(lamb, muM, nfreqs, Ks, Ka, Ca, Z, I)

    maxDamping = np.max(damping, axis = 1)
    signChange = np.where(np.diff(np.sign(maxDamping)))[0][0]
    boundary = np.interp(0, maxDamping[signChange:(signChange + 2)],
                     lambdaVec[signChange:(signChange + 2)])

    return boundary


# Model setup
muM = 0.1
m = 6
Cag = np.eye(m)
Zg = np.zeros((m, m))
Ig = np.eye(m)

nHh = 21
Hhvec = np.linspace(0, 5.0, nHh)
lambdaVec = np.linspace(10, 600, 100)


boundaryNoStatic = np.zeros((nHh, ))
boundaryFlutter = np.zeros((nHh, ))
divergence = np.zeros((nHh, ))

for ind, Hh in enumerate(Hhvec):

    print('Running H/h = %.1f (%i/%i)' % (Hh, ind + 1, nHh))
    # Galerkin model
    Gammax = 8*Hh
    Ksg, Kag, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)

    boundaryFlutter[ind], divergence[ind], freq, damping = criticalBoundariesStatic(lambdaVec, muM, Ksg, Kag,
                   Cag, Zg, Ig, Gammax, fs, Ipi2, oneMinusOneM)
    boundaryNoStatic[ind] = flutterBoundary(lambdaVec, muM, Ksg, Kag, Cag, Zg, Ig)


largeDispHh = [1.0, 2.0, 3.5, 5.0]
largeDispBounds = [257, 205, 149, 146]
abaqusBounds = [245.26827328634786, 200.55687422023425, 196.2500065287108, 193.3144570147281]


#%% Plotting
# This one plots the large disp results
# Set up figure

# Some nicer colors
blue = lambda alpha: [0.1, 0.2, 0.7, alpha]
red = lambda alpha: [0.7, 0.2, 0.2, alpha]

fig = plt.figure(figsize = (12, 6))

plt.plot(dowell_withStaticHh, dowell_withStaticLambda, 'ks', markeredgewidth = 2.0,
         label = 'Dowell; Large-Displacement',
         markerfacecolor = (0.0, 0.0, 0.0, 0.45), markeredgecolor = (0.0, 0.0, 0.0, 1.0))


plt.plot(largeDispHh, largeDispBounds, 'o', markeredgewidth = 2.0, markersize = 8,
         label = 'Present; Large-Displacement; Galerkin',
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))
plt.plot(Hhvec, boundaryFlutter, '.-', label = 'Present; Small-Displacement [Flutter]', color = blue(1.0),
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))
plt.plot(Hhvec, divergence, '--', label = 'Present; Small-Displacement [Divergence]', color = blue(1.0),
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))


plt.xlabel('Rise Ratio H/h', fontsize = 14, fontweight = 'bold')
plt.ylabel('Flutter Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
plt.legend(fontsize = 13)
plt.xlim([-0.05, 5.05])

helper.save(fig, 'images/large/flutterVsHH_largeDisp.svg', 8, 16)


#%% This one plots the large disp results + Abaqus results

# Some nicer colors
blue = lambda alpha: [0.1, 0.2, 0.7, alpha]
red = lambda alpha: [0.7, 0.2, 0.2, alpha]

fig = plt.figure(figsize = (12, 6))

plt.plot(dowell_withStaticHh, dowell_withStaticLambda, 'ks', markeredgewidth = 2.0,
         label = 'Dowell; Large-Displacement',
         markerfacecolor = (0.0, 0.0, 0.0, 0.45), markeredgecolor = (0.0, 0.0, 0.0, 1.0))


plt.plot(largeDispHh, largeDispBounds, 'o', markeredgewidth = 2.0, markersize = 8,
         label = 'Present; Large-Displacement; Galerkin',
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))

plt.plot(largeDispHh, abaqusBounds, 'o', markeredgewidth = 2.0,
         label = 'Present; Abaqus Convergence',
         markerfacecolor = red(0.45), markeredgecolor = red(1.0))

plt.plot(Hhvec, boundaryFlutter, '.-', label = 'Present; Small-Displacement [Flutter]', color = blue(1.0),
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))
plt.plot(Hhvec, divergence, '--', label = 'Present; Small-Displacement [Divergence]', color = blue(1.0),
         markerfacecolor = blue(0.45), markeredgecolor = blue(1.0))




plt.xlabel('Rise Ratio H/h', fontsize = 14, fontweight = 'bold')
plt.ylabel('Flutter Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
plt.legend(fontsize = 13)
plt.xlim([-0.05, 5.05])

helper.save(fig, 'images/large/fig05_flutterVsHH_abaqus.svg', 8, 16)

