# -*- coding: utf-8 -*-
"""

Makes plots relating to the flat panel dynamics for the Galerkin and the ROM.

"""

import helper
helper.addpaths()
import os
import numpy as np
import sys
import matplotlib.pyplot as plt

import scipy
import scipy.io

# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness

# File loading
matfile = 'figure3.mat'
matdat = scipy.io.loadmat(os.path.join('digitized', matfile))

# Digitization of data
X1Y0 = np.array(matdat['X1Y0'][0], dtype = float)
X0Y1 = np.array(matdat['X0Y1'][0], dtype = float)
X0 = X0Y1[0]
Y0 = X1Y0[1]
DX = X1Y0[0] - X0
DY = X0Y1[1] - Y0

x = np.array(matdat['xrange'][0], dtype = float); dx = x[1] - x[0]
y = np.array(matdat['yrange'][0], dtype = float); dy = y[1] - y[0]

pix2plot = lambda X, Y: (dx*(X - X0)/DX + x[0], dy*(Y - Y0)/DY + y[0])
dowell_noStaticHh, dowell_noStaticLambda = pix2plot(matdat['nostatic'][:, 0], matdat['nostatic'][:, 1])

#dowell_withStaticHh = np.array([0, 1, 2, 3.5, 5])
#dowell_withStaticLambda = np.array([350, 280, 180, 170, 150])
#dowell_noStaticHh = np.array([0, 1, 2, 3.5, 5])
#dowell_noStaticLambda = np.array([350, 180, 110, 410, 500])

# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()


# Compute freq/damping for Galerkin model
def freqDampGal(lamb, muM, nfreqs, Ksg, Kag, Cag, Zg, Ig):

    # Compute damping value
    gdamp = np.sqrt(muM*lamb)

    A = np.bmat([[Zg, Ig], [-2*lamb*Kag - 2*Ksg, -gdamp*Cag]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    return np.abs(np.imag(v[inds[:nfreqs]])), np.real(v[inds[:nfreqs]])


def flutterBoundaryGal(lambdaVec, muM, Ks, Ka, Ca, Z, I):
    '''Obtain flutter boundary in terms of mu/M values for given system matrices.'''
    n = len(lambdaVec)
    boundary = np.zeros((n, ))
    nfreqs = 2

    damping = np.zeros((n, nfreqs))
    for ind, lamb in enumerate(lambdaVec):
        freq, damping[ind, :] = freqDampGal(lamb, muM, nfreqs, Ks, Ka, Ca, Z, I)

    maxDamping = np.max(damping, axis = 1)
    signChange = np.where(np.diff(np.sign(maxDamping)))[0][0]
    boundary = np.interp(0, maxDamping[signChange:(signChange + 2)],
                     lambdaVec[signChange:(signChange + 2)])

    return boundary

# Parameters for non-dimensionalizing
gamma = 1.4
E = 7.1E10
rho = 2.7E3
h = 1E-3
nu = 0.33
a = 0.5
mInf = 10.0
De = E*h**3/(12*(1 - nu**2))
t2tau = 1/np.sqrt(rho*h*a**4/De)

def setpInf(lamb):
    """
    Set the required static pressure to obtain a desired lambda given the freestream Mach number,
    specific heat ratio gamma, panel length a, and panel rigidity D.
    """
    return lamb * np.sqrt(mInf*mInf - 1)/(mInf*mInf) * (De/(gamma*a*a*a))


def setaInf(muM, pInf):
    """
    Set the freestream speed of sound to obtain a desired rati of mu/M.
    """
    return np.sqrt(1/muM * (pInf/mInf) * (a*gamma/(rho*h)))


# Compute freq/damping for Abaqus model
def freqDampAb(lamb, muM, nfreqs, Ka, Ca, Omega):

    # Compute static pressure and speed of sound
    pinf = setpInf(lamb)
    ainf = setaInf(muM, pinf)
    qinf = 0.5*gamma*pinf*mInf*mInf
    beta = np.sqrt(mInf*mInf - 1)
    U = mInf*ainf

    Kal = 2*qinf/beta*Ka
    Cal = 2*qinf/beta*(mInf*mInf - 2)/(mInf*mInf - 1)/U*Ca

    m = Ka.shape[0]
    Z = np.zeros((m, m))
    I = np.eye(m)

    A = np.bmat([[Z, I], [Kal - Omega, Cal]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    return np.abs(np.imag(v[inds[:nfreqs]])), np.real(v[inds[:nfreqs]])
    # return v


def flutterBoundaryAb(lambdaVec, muM, A, B, D, phi, fn):
    '''
    Obtain flutter boundary for model at a given lambda.
    '''
    nm = 6
    A = arrays['A']
    B = arrays['B']
    D = arrays['D']
    phi = modal['phi'][:, :nm]
    fn = modal['fn'].flatten()[:nm]

    n = len(lambdaVec)

    boundary = np.zeros((n, ))
    nfreqs = 25

    Ka = phi.T @ D @ A @ phi
    Ca = phi.T @ D @ B @ phi
    Omega = np.diag((fn*2.0*np.pi)**2)

    damping = np.zeros((n, nm))
    # v = np.zeros((n, nm), dtype = np.complex)
    for ind, lamb in enumerate(lambdaVec):
        freq, damping[ind, :] = freqDampAb(lamb, muM, nfreqs, Ka, Ca, Omega)
        # v[ind, :] = freqDampAb(lamb, muM, nfreqs, Ka, Ca, Omega)

    maxDamping = np.max(damping, axis = 1)
    signChange = np.where(np.diff(np.sign(maxDamping)))[0][0]
    boundary = np.interp(0, maxDamping[signChange:(signChange + 2)],
                     lambdaVec[signChange:(signChange + 2)])

    return boundary

# Abaqus loading setup
loadpath = r'..\abaqus_data\undeformed_dynamics'
casename = lambda Hh: ('Hh_%.2f' % Hh).replace('.', 'pt')
fullname1 = lambda Hh: os.path.join(loadpath, casename(Hh), 'WETTED_SURFACE.mat')
fullname2 = lambda Hh: os.path.join(loadpath, casename(Hh), 'WETTED_SURFACE_arrays.mat')


# Model setup
muM = 0.1
m = 6
Cag = np.eye(m)
Zg = np.zeros((m, m))
Ig = np.eye(m)

nHh = 21
Hhgal = np.linspace(0, 5.0, nHh)
lambdaVec = np.linspace(10, 600, 100)
boundaryGal = np.zeros((nHh, ))


# Galerkin boundaries
for ind, Hh in enumerate(Hhgal):

    print('Running H/h = %.1f (%i/%i)' % (Hh, ind + 1, nHh))
    # Galerkin model
    Gammax = 8*Hh
    Ksg, Kag, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    boundaryGal[ind] = flutterBoundaryGal(lambdaVec, muM, Ksg, Kag, Cag, Zg, Ig)

# Abaqus boundaries
Hhab = np.array([0.0, 1.0, 2.0, 3.5, 5.0])
nHh = len(Hhab)
boundaryAb = np.zeros((nHh, ))
for ind, Hh in enumerate(Hhab):
    # Abaqus model
    modal = scipy.io.loadmat(fullname1(Hh))
    arrays = scipy.io.loadmat(fullname2(Hh))
    boundaryAb[ind] = flutterBoundaryAb(lambdaVec, muM, arrays['A'], arrays['B'], arrays['D'],
              modal['phi'], modal['fn'])

#%% Plotting
# This one plots only the base state results
# Set up figure

# Some nicer colors
galerkinColor = (0.2, 0.2, 0.6, 1.0)
abaqusEdgeColor = (0.6, 0.2, 0.2, 1.0)
abaqusFaceColor = (0.6, 0.2, 0.2, 0.75)
refFaceColor = (0.0, 0.0, 0.0, 0.75)
refEdgeColor = (0.0, 0.0, 0.0, 1.0)

fig = plt.figure(figsize = (12, 6))
plt.plot(dowell_noStaticHh, dowell_noStaticLambda, 'ko', markeredgewidth = 2.0,
         label = 'Dowell; No Static Aero Load',
         markerfacecolor = refFaceColor, markeredgecolor = refEdgeColor)

plt.plot(Hhgal, boundaryGal, '.-', label = 'Present: Galerkin; No Static Aero Load', color = galerkinColor)
plt.plot(Hhab, boundaryAb, '^', label = 'Present: Abaqus/ROM; No Static Aero Load',
         markerfacecolor = abaqusFaceColor, markeredgecolor = abaqusEdgeColor)

plt.xlabel('Rise Ratio H/h', fontsize = 14, fontweight = 'bold')
plt.ylabel('Flutter Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
plt.legend(fontsize = 13)
plt.xlim([-0.05, 5.05])


helper.save(fig, 'images/large/fig03_undeformed_flutterBoundary.svg', 8, 16)
# helper.save(fig, 'images/small/figXX_undeformed_flutterBoundary.png', 5, 12)


