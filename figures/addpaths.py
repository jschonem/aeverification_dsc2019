# -*- coding: utf-8 -*-
"""

Place important directories on the search path.

"""

import sys


def addpaths():
    pathlist = ['../galerkin' ]

    for path in pathlist:
        if path not in sys.path:
            sys.path.append(path)