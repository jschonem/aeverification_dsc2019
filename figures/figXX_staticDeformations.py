# -*- coding: utf-8 -*-
"""

Generate a comparison of static deformations between Abaqus and a 6-mode Galerkin model.

"""

import helper
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize
import scipy.io

helper.addpaths()

from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness


# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()


# Obtain the static aeroelastic deflection of a panel based on the aerodynamic loading
def staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 100, n = 10):

    q0 = np.zeros((Ks.shape[0], ))
    loadfrac = np.linspace(0, 1, n)
    for i in range(n):
        load = lamb*loadfrac[i]
        fun = lambda q: np.linalg.norm(load*fs
                                       + ((load*Ka + Ks) @ q + nlForce(q, Gammax, Ipi2, oneMinusOneM)))
        sol = scipy.optimize.minimize(fun, q0)
        q0 = sol.x
    xi = np.linspace(0, 1, nxi)
    # Get the physical deflection
    w = np.zeros((nxi, ))
    for i, q in enumerate(q0):
        w += q*np.sin((i + 1)*np.pi*xi)

    return q0, w

# Function to compute undeformed shape of panel
x = np.linspace(0, 1, 80)
zfun = lambda hh: hh*(1 - (x - 0.5)**2/(0.5)**2)

# Function to load Abaqus data and return quantities of interest
def getAbaqusData(path, hh):
    name = 'hh%.0fpt%.0f' % (np.floor(hh), 10*(hh - np.floor(hh)))
    fullpath = os.path.join(path, name, 'staticJob_staticStep.pkl')
    with open(fullpath, 'rb') as fid:
        data = pickle.load(fid, encoding = 'latin1')

    sortInds = np.argsort(data['nodeLabel'])


    # Also get the ramped pressure value from the setup file
    fullpath = os.path.join(path, name, 'setup')
    with open(fullpath, 'rt') as fid:
        for line in fid.readlines():
            token = line.split('=')
            if token[0] == 'lambdamax':
                lambdamax = float(token[-1])
            if token[0] == 'Nex':
                Nex = int(token[-1])
            if token[0] == 't':
                t = float(token[-1])

    lambVec = data['time']*lambdamax
    u3 = data['U3'][:, sortInds[:Nex]]/t

    return u3, lambVec

# Properties
abaqusPath = r'..\abaqus_data\static_deformations'
m = 6

# Plotting. Frames to plot are selected manually.
hh = 5.0
framePlotIndices = [25, 75, 108]

fig, ax = plt.subplots(2, 2)
ax = ax.flatten()

for ind, plotIndex in enumerate(framePlotIndices):

    # Undeformed shape of panel
    z = zfun(hh)

    # Get Abaqus data
    u3, lambVec = getAbaqusData(abaqusPath, hh)
    lamb = lambVec[plotIndex]
    ab = z + u3[plotIndex, :]

    # Get Galerkin displacement
    Gammax = 8*hh
    Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    q0, w = staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = len(x), n = 10)
    gal = z + w

    # Plotting
    helper.setColorCycle(ax[ind])

    ax[ind].plot(x, z, 'k:')
    ax[ind].plot(x, gal, label = 'Galerkin', linewidth = 2.0)
    ax[ind].plot(x, ab, label = 'Abaqus', linewidth = 2.0)

    ax[ind].set_title('$\mathbf{\lambda = %.0f}$' % lamb, fontsize = 14, fontweight = 'bold')
    ax[ind].set_ylim([0, ax[ind].get_ylim()[-1]])
    ax[ind].set_xlim([0, 1])


# Also plot the snapped-through Galerkin solution
lamb = 300
# Get Galerkin displacement
Gammax = 8*hh
Ks, Ka, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
q0, w = staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = len(x), n = 10)
gal = z + w
ax[-1].plot(x, z, 'k:')
ax[-1].plot(x, gal, label = 'Galerkin', linewidth = 2.0)
ax[-1].set_title('$\mathbf{\lambda = %.0f}$' % lamb, fontsize = 14, fontweight = 'bold')

ax[2].set_xlabel('$\\xi = x/L$', fontsize = 14, fontweight = 'bold')
ax[3].set_xlabel('$\\xi = x/L$', fontsize = 14, fontweight = 'bold')
ax[0].set_ylabel('w/h', fontsize = 14, fontweight = 'bold')
ax[2].set_ylabel('w/h', fontsize = 14, fontweight = 'bold')
ax[0].legend(fontsize = 13)

#helper.save(fig, '../figures/images/small/figXX_staticDeformations.png', 3, 12)
helper.save(fig, '../figures/images/large/figXX_staticDeformations.png', 12, 16)



