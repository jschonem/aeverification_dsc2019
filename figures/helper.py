# -*- coding: utf-8 -*-
"""

Place important directories on the search path.

"""

import sys
import matplotlib as mpl

def addpaths():
    pathlist = ['../galerkin' ]

    for path in pathlist:
        if path not in sys.path:
            sys.path.append(path)

def setColorCycle(ax):

    red = [0.8, 0.2, 0.2]
    blue = [0.2, 0.2, 0.8]
    black = [0, 0, 0]

    dc = mpl.rcParams['axes.prop_cycle'].by_key()['color']
    # x = mpl.cycler('color', [red, blue, black])
    # ax.set_prop_cycle('color', [red, blue, black])
    ax.set_prop_cycle('color', [dc[0], dc[3], dc[7]])

def save(fig, path, height, width):

    fig.set_figheight(height)
    fig.set_figwidth(width)

    fig.savefig(path, transparent = True, bbox_inches = 'tight')
