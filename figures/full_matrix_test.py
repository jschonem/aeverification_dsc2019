# -*- coding: utf-8 -*-
"""

Makes plots relating to the flat panel dynamics for the Galerkin and the ROM.

"""

import helper
helper.addpaths()

import os
import pickle
import numpy as np
import sys
import matplotlib.pyplot as plt

import scipy
import scipy.io
# TODO: The ROM has to write out and save results in separate files, because it can't be distributed
# along with the other files.

# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness


# Define physical properties
E = 71000E6
rho = 2700
nu = 0.33
t = 0.001
Lx = 0.5
De = E*t**3/(12*(1 - nu**2))

# Non-dimensional parameters
Hh = 5.0
mInf = 10.0
mu = 0.1*mInf
tau2t = np.sqrt(rho*t*Lx**4/De)
T = 1.0
n = 1000
tau = np.linspace(0, T/tau2t, int(n*T))
gamma = 1.4

def setpInf(lamb, mInf, gamma, a, D):
    """
    Set the required static pressure to obtain a desired lambda given the freestream Mach number,
    specific heat ratio gamma, panel length a, and panel rigidity D.
    """

    return lamb * np.sqrt(mInf*mInf - 1)/(mInf*mInf) * (De/(gamma*a*a*a))


def setaInf(muM, pInf, mInf, a, gamma, rhoM, t):
    """
    Set the freestream speed of sound to obtain a desired rati of mu/M.
    """

    return np.sqrt(1/muM * (pInf/mInf) * (a*gamma/(rhoM*t)))



# Function to compute frequency/damping for the ROM
def freqDampRom(nfreqs, Omega, Ka, Ca, Z, I):
    # Obtain equivalent pressure and speed of sound
    As = np.bmat([[Z, I], [Ka - Omega, Ca]])
    v = np.linalg.eigvals(As)[::2]
    inds = np.argsort(np.imag(v))

    v = v[inds[:nfreqs]]
    wd = np.abs(np.imag(v))
    n = np.real(v)
    z = n/wd
    z = n/wd
    z[wd == 0] = n[wd == 0]

    return wd/(2*np.pi), z
#
#def freqDampFull(nfreqs, K, M, Ka, Ca, Z, I):
#    # Obtain equivalent pressure and speed of sound
#    As = np.bmat([[Z, I], [Ka - Omega, Ca]])
#    v = np.linalg.eigvals(As)[::2]
#    inds = np.argsort(np.imag(v))
#
#    v = v[inds[:nfreqs]]
#    wd = np.abs(np.imag(v))
#    n = np.real(v)
#    z = n/wd
#    z = n/wd
#    z[wd == 0] = n[wd == 0]
#
#    return wd/(2*np.pi), z



#%% Static dynamics - Abaqus

filepath = r'../abaqus_data/deformed_dynamics/hh_5pt0'
file = 'session_WETTED_SURFACE.pkl'

with open(os.path.join(filepath, file), 'rb') as fid:
    data = pickle.load(fid)[0]

matrixfile = 'matrices_lastframe.mat'
bigmats = scipy.io.loadmat(os.path.join(filepath, matrixfile))
K = bigmats['K']
M = bigmats['M']
n = K.shape[0]


# Statically reduce out the translational DOF
#i = np.sort(np.hstack([np.arange(3, n, 6), np.arange(4, n, 6), np.arange(5, n, 6)]))
b = np.sort(np.hstack([np.arange(0, n, 6), np.arange(1, n, 6), np.arange(2, n, 6)]))
i = np.arange(4, n, 6)
# b = np.sort(np.hstack([np.arange(0, n, 6), np.arange(2, n, 6)]))
Kbb = K[b, :][:, b]
Kii = K[i, :][:, i]
Kbi = K[b, :][:, i]
Mbb = M[b, :][:, b]
Mii = M[i, :][:, i]
Mbi = M[b, :][:, i]
tau = -scipy.sparse.linalg.inv(Kii) @ Kbi.T

Ksort = scipy.sparse.bmat([[Kbb, Kbi], [Kbi.T, Kii]]).tocsc()
Msort = scipy.sparse.bmat([[Mbb, Mbi], [Mbi.T, Mii]]).tocsc()

psi = scipy.sparse.vstack([scipy.sparse.eye(len(b)), tau]).tocsc()

Kred = (psi.T @ Ksort @ psi).tocsc()
Mred = (psi.T @ Msort @ psi).tocsc()

# v = scipy.sparse.linalg.eigs(Kred, k = 6, M = Mred, sigma = 1.0, which = 'LM', return_eigenvectors = False)
# v = scipy.sparse.linalg.eigs(K, k = 6, M = M, sigma = 1.0, which = 'LM', return_eigenvectors = False)
In = scipy.sparse.eye(len(b)).tocsc()

# ROM parameters
lambmax = 200.0
muM = 0.1
N = 20

nm = 20
Z = np.zeros((nm, nm))
I = np.eye(nm)

nfreqs = 4
fnab = np.zeros((nfreqs, N))
zab = np.zeros((nfreqs, N))
lambvec = np.linspace(1, lambmax, N)
fnbig = np.zeros((nfreqs, N))
zbig = np.zeros((nfreqs, N))


system = data[-1]
A = system['A']
B = system['B']
D = system['D']
omega = system['omega'][:nm]
phi = system['phi'][:, :nm]




Minv = scipy.sparse.linalg.inv(Mred)
for ind, lamb in enumerate(lambvec):


    Omega = np.diag(omega**2)
    pInf = setpInf(lamb, mInf, gamma, Lx, De)
    aInf = setaInf(muM, pInf, mInf, Lx, gamma, rho, t)
    U = mInf*aInf
    stiffnessCoeff = pInf*gamma*mInf**2/np.sqrt(mInf**2 - 1)
    dampingCoeff = stiffnessCoeff*((mInf**2 - 2)/(mInf**2 - 1))/U

    Ka = stiffnessCoeff*phi.T @ D @ A @ phi # Full set of coefficients included
    Ca = dampingCoeff*phi.T @ D @ B @ phi # Full set of coefficients included

    fnab[:, ind], zab[:, ind] = freqDampRom(nfreqs, Omega, Ka, Ca, Z, I)

    Ka = stiffnessCoeff*D @ A # Full set of coefficients included
    Ca = dampingCoeff*D @ B # Full set of coefficients included
    As = scipy.sparse.bmat([[None, In], [Minv @ (Ka - Kred), Minv @ Ca]]).tocsc()
    va = scipy.sparse.linalg.eigs(As, k = 2*nfreqs, sigma = 0.0, which = 'LM', return_eigenvectors = False)
    v = va[np.imag(va) >= 0]
    inds = np.argsort(np.imag(v))

    v = v[inds[:nfreqs]]
    wd = np.abs(np.imag(v))
    n = np.real(v)
    z = n/wd
    z = n/wd
    z[wd == 0] = n[wd == 0]

    fnbig[:, ind] = wd/(2*np.pi)
    zbig[:, ind] = z



#%% Plotting
# This one plots both sets of results
# Set up figure

# Some nicer colors
blue = lambda alpha: [0.1, 0.2, 0.7, alpha]
red = lambda alpha: [0.7, 0.2, 0.2, alpha]

fig, ax = plt.subplots(2, 1)
ax = ax.flatten()


ax[0].plot(lambvec, fnab[0, :].T, 'o')
ax[0].plot(lambvec, fnbig[0, :].T)

ax[1].plot(lambvec, zab.T, 'o')
ax[1].plot(lambvec, zbig.T)

# plt.plot(Hhvec, divergence, label = 'Divergence')
# ax[0].set_ylim([0, 100])



#plt.xlabel('Rise Ratio H/h', fontsize = 14, fontweight = 'bold')
#plt.ylabel('Flutter Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
# plt.legend(fontsize = 13)
#plt.xlim([-0.05, 5.05])
#plt.savefig('images/flutterVsHH_both.png', bbox_inches = 'tight', transparent = True)
