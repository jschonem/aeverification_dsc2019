# -*- coding: utf-8 -*-
"""

Compares Abaqus to analytical integration; Hh = 1.5; lambda = 500; mu = 0.1. Midpoint response.

"""

import helper
import pickle
import numpy as np
import matplotlib.pyplot as plt
import os
import scipy.io

helper.addpaths()
#sys.path.append('../integrations')

#import runIntegration


# Non-dimensional parameters
dtau = 1.0 # Look at last dtau units for Pyhton
dtauab = 1.0 ## Look at last datu units for Abaqus

nx = 100 # Number of spatial samples for finding max value
Hh = 1.5

# xi = np.linspace(0, 1, nx)
xi = np.array([0.75])
nx = 1

# Parameters for non-dimensionalizing
E = 7.1E10
rho = 2.7E3
h = 1E-3
nu = 0.33
a = 0.5
D = E*h**3/(12*(1 - nu**2))
t2tau = 1/np.sqrt(rho*h*a**4/D)


digiPath = 'digitized'
digiDat = scipy.io.loadmat(os.path.join(digiPath, 'figure4.mat'))

size = np.array(digiDat['figsize'][0][2:], dtype = float)

X0Y0 = np.array(digiDat['X0Y0'][0], dtype = float)
X1Y0 = np.array(digiDat['X1Y0'][0], dtype = float)
X0Y1 = np.array(digiDat['X0Y1'][0], dtype = float)
X0 = X0Y0[0]
Y0 = X0Y0[1]
DX = X1Y0[0] - X0
DY = X0Y1[1] - Y0

x = np.array(digiDat['X'][0], dtype = float); dx = x[1] - x[0]
y = np.array(digiDat['Y'][0], dtype = float); dy = y[1] - y[0]

pix2plot = lambda X, Y: (dx*(X - X0)/DX + x[0], dy*(Y - Y0)/DY + y[0])
resultsPath = '../python_data/lco_integration'
filestr = lambda hh: ('Hh%.2f' % hh).replace('.', 'pt') + '.pkl'
abfilestr = lambda lamb: ('explicit_%i' % lamb)
Hh = [0.0, 1.0, 2.0, 5.0]
labels = ['Hh0', 'Hh1', 'Hh2', 'Hh5']

stabCodes = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],        # (H/h = 0.0)
             [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],        # (H/h = 1.0)
             [0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1],        # (H/h = 2.0)
             [0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]  # (H/h = 5.0)

abStabCodes = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],        # (H/h = 0.0)
             [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],        # (H/h = 1.0)
             [0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1],        # (H/h = 2.0)
             [0, 0, 0, 2, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]  # (H/h = 5.0)


stabTypes = ['Stable (G)', 'LCO (G)', 'Chaotic (G)']
abStabTypes = ['Stable (A)', 'LCO (A)', 'Chaotic (A)']
stabIcons = ['^', 'o', 'x']


fig, ax = plt.subplots(2, 2)
fig.set_figheight(8)
fig.set_figwidth(16)
ax = ax.flatten()
xi = 0.75
m = 6
phi = np.zeros((m, ))
for i in range(m):
    phi[i] = np.sin((i + 1)*np.pi*xi)


# Python plotting
for ind, hh in enumerate(Hh):

    with open(os.path.join(resultsPath, filestr(hh)), 'rb') as fid:
        data = pickle.load(fid)
        tau = data['tau']
        keepinds = tau > ((data['Tau']) - dtau)
        taukeep = tau[keepinds]
        Wmax = list()
        k = 0

        for lamb, q in data['results'].items():

            q = q[keepinds, :m]

            w = phi @ q.T
            Wmax.append(np.max(np.abs(w)))
            stabCode = stabCodes[ind][k]
            ax[ind].plot(lamb, np.max(np.abs(w)), stabIcons[stabCode],
              color = (0.2, 0.2, 0.6, 1.0),
              markerfacecolor = (0.2, 0.2, 0.6, 0.75), markeredgecolor = (0.2, 0.2, 0.6, 1.0))
            k+=1

        # Add digitized data
        dig = np.array(digiDat[labels[ind]])
        a, b = pix2plot(dig[:, 0], dig[:, 1])
        marker = ax[ind].plot(a, b, 'ks', markeredgewidth = 2.0, label = 'Dowell',
                   markerfacecolor = (0.0, 0.0, 0.0, 0.45), markeredgecolor = (0.0, 0.0, 0.0, 1.0))

        if ind == 0:
            for i in range(3):
                ax[ind].plot(-1000, np.max(np.abs(w)), stabIcons[i], label = stabTypes[i],
              color = (0.2, 0.2, 0.6, 1.0),
              markerfacecolor = (0.2, 0.2, 0.6, 0.75), markeredgecolor = (0.2, 0.2, 0.6, 1.0))

#        for m in marker:
#            m.set_markerfacecolor((0.0, 0.0, 0.0, 0.5))
#            m.set_markeredgecolor((0.0, 0.0, 0.0, 1.0))
        ax[ind].set_xlim([35, 1015])
        ax[ind].set_title('H/h = %.1f' % hh, fontsize = 14, fontweight = 'bold')

    # Abaqus plotting
    if True:
        # Read files
        fileCode = 'hh%.0fpt%.0f' % (round(hh), 10*(hh - round(hh)))
        abResultsPath = '../abaqus_data/direct_integration/explicitVDLOAD/%s/results' % fileCode
        files = os.listdir(abResultsPath)
        lambs = [int(s.split('_')[-1]) for s in files]
        lambs.sort()
        key = 'Node PANEL.223_U3'
        k = 0

        for lamb in lambs:
            with open(os.path.join(abResultsPath, abfilestr(lamb)), 'rb') as fid:
                data = pickle.load(fid, encoding = 'latin1')

            tau = data[key][:, 0]*t2tau
            w = data[key][:, 1]/h
            Tau = tau[-1]

            keepinds = tau > (Tau - dtauab)

            # stabCode = stabCodes[k]
            # xlim = ax[k].get_xlim()
            stabCode = abStabCodes[ind][k]
            ax[ind].plot(lamb, np.max(np.abs(w[keepinds])), stabIcons[stabCode],
              color = (0.8, 0.2, 0.2, 1.0), markersize = 4,
              markerfacecolor = (0.8, 0.2, 0.2, 0.75), markeredgecolor = (0.8, 0.2, 0.2, 1.0))
            k += 1

        if ind == 0:
            for i in range(3):
                ax[ind].plot(-1000, np.max(np.abs(w)), stabIcons[i], label = abStabTypes[i],
              color = (0.8, 0.2, 0.2, 1.0), markersize = 4,
              markerfacecolor = (0.8, 0.2, 0.2, 0.75), markeredgecolor = (0.8, 0.2, 0.2, 1.0))




# Add format items
ax[0].legend(fontsize = 13, loc = 2)
ax[0].set_ylabel('$\mathbf{W_{max}}$', fontsize = 14)
ax[2].set_ylabel('$\mathbf{W_{max}}$', fontsize = 14)

ax[2].set_xlabel('$\mathbf{\lambda}$', fontsize = 14)
ax[3].set_xlabel('$\mathbf{\lambda}$', fontsize = 14)

# helper.save(fig, '../figures/images/large/LCOCompare_G.svg', 8, 16)
helper.save(fig, '../figures/images/large/fig08_LCOCompare_GA.svg', 8, 16)