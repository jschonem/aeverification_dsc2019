# -*- coding: utf-8 -*-
"""

Demonstrate Abaqus and Galerkin natural frequency evolution with plate curvature.

"""

import pickle
import numpy as np
import matplotlib.pyplot as plt
import helper
import scipy.io

# Pickle file location of Abaqus results
pickleFile = '../abaqus_data/frequencyConvergence/pickles/summary.pkl'

# Load pickle file and assign physical parameters
with open(pickleFile, 'rb') as fid:
    data = pickle.load(fid)

E = data['E']
rho = data['rho']
t = data['t']
Lx = data['Lx']
nu = data['nu']


D = E*t**3/(12*(1 - nu*nu))
t2tau = np.sqrt(D/(rho*t*Lx**4))


def getGalerkinFreqs(Hh, nFreqs, m):
    '''Return the first nFreqs natural frequencies of an m-mode model.'''

    # Structural stiffness matrix
    Gammax = 8*Hh
    Ks = np.zeros((m, m))
    for i in range(1, m + 1):
        for j in range(1, m + 1):
            if i == j:
                Ks[i - 1, j - 1] = (i*np.pi)**4
            Ks[i - 1, j - 1] += 24*Gammax**2/(j*i*np.pi**2)*(1 - (-1)**i)*(1 - (-1)**j)

    v = np.linalg.eigvalsh(Ks)
    return np.sqrt(v[:nFreqs])*t2tau/(2*np.pi)


m = 6
nFreqs = 4
nG = 100
HhG = np.linspace(0, 10, nG)
fnG = np.zeros((nG, nFreqs))

for ind, Hh in enumerate(HhG):
    fnG[ind, :] = getGalerkinFreqs(Hh, nFreqs, m)


# Make figure
fig = plt.figure(figsize = (5, 8))
g = plt.plot(HhG, fnG/fnG[0, 0], 'b')[0].set_label('%i Mode Galerkin' % m)

plt.plot(data['hhVec'], data['fnVec'][:, :nFreqs]/fnG[0, 0], 's',
         markeredgecolor = [0, 0, 0, 1],
         markerfacecolor = [0, 0, 0, 0.6])[0].set_label('Abaqus')


plt.legend(fontsize = 13)
plt.xlabel('Rise Ratio H/h', fontsize = 14, fontweight = 'bold')
plt.ylabel('Natural Frequencies $\\mathbf{\\omega/\\omega_0}$',
           fontsize = 14, fontweight = 'bold')
plt.xlim([0, 10])

helper.save(fig, 'images/large/fig02_naturalFrequencies.svg', 8, 5)
