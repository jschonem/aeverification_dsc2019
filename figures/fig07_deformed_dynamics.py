# -*- coding: utf-8 -*-
"""

Makes plots relating to the flat panel dynamics for the Galerkin and the ROM.

"""

import helper
helper.addpaths()

import os
import pickle
import numpy as np
import sys
import matplotlib.pyplot as plt

import scipy
import scipy.io
# TODO: The ROM has to write out and save results in separate files, because it can't be distributed
# along with the other files.

# Import Galerkin model functions
galerkinPath = '../integrations'
if galerkinPath not in sys.path:
    sys.path.append(galerkinPath)
from galerkinIntegrationPython import prepVecMatrices
from galerkinIntegrationPython import integrate_vec
from galerkinIntegration import getTangentStiffness


# Define physical properties
E = 71000E6
rho = 2700
nu = 0.33
t = 0.001
Lx = 0.5
De = E*t**3/(12*(1 - nu**2))

# Non-dimensional parameters
Hh = 5.0
mInf = 10.0
mu = 0.1*mInf
tau2t = np.sqrt(rho*t*Lx**4/De)
T = 1.0
n = 1000
tau = np.linspace(0, T/tau2t, int(n*T))
gamma = 1.4

def setpInf(lamb, mInf, gamma, a, D):
    """
    Set the required static pressure to obtain a desired lambda given the freestream Mach number,
    specific heat ratio gamma, panel length a, and panel rigidity D.
    """

    return lamb * np.sqrt(mInf*mInf - 1)/(mInf*mInf) * (D/(gamma*a*a*a))


def setaInf(muM, pInf, mInf, a, gamma, rhoM, t):
    """
    Set the freestream speed of sound to obtain a desired rati of mu/M.
    """

    return np.sqrt(1/muM * (pInf/mInf) * (a*gamma/(rhoM*t)))


# This function defines the nonlinear force at a given displacement
def nlForce(q, Gammax, Ipi2, oneMinusOneM):
    qIpi2 = q*Ipi2
    Knl2 = 1.5*(qIpi2).T @ (qIpi2)
    Knl1a = 6*Gammax*(qIpi2).T @ oneMinusOneM
    fnl1b = Gammax*3*np.sum(q*qIpi2)*oneMinusOneM

    return (Knl2 + Knl1a) @ q + fnl1b.flatten()

# Obtain the static aeroelastic deflection of a panel based on the aerodynamic loading
def staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 100):
    fun = lambda q: np.linalg.norm(lamb*fs +
                                   ((lamb*Ka + Ks) @ q + nlForce(q, Gammax, Ipi2, oneMinusOneM)))
    sol = scipy.optimize.minimize(fun, np.zeros((m, )))
    q0 = sol.x
    xi = np.linspace(0, 1, nxi)
    # Get the physical deflection
    w = np.zeros((nxi, ))
    for i, q in enumerate(q0):
        w += q*np.sin((i + 1)*np.pi*xi)

    return q0, w

# Obtain nolinear portion of the tangent stiffness matrix at a given deflection
def tangentStiffness(q, Gammax):
    m = len(q)
    Kt = np.zeros((m, m))
    for i in range(m):
        for j in range(m):
            S = i + 1
            M = j + 1

            # First part
            Kt[i, j] = 3*(M*S*np.pi*np.pi)**2*q[i]*q[j]
            if i == j:
                for k in range(m):
                    Kt[i, j] += 1.5*(q[k]*np.pi**2*(k + 1)*S)**2

            # Second part
            Kt[i, j] += 6*Gammax*q[i]*(S*np.pi)**2*(1 - np.power(-1, M))/(M*np.pi)
            if i == j:
                for k in range(m):
                    Kt[i, j] += 6*Gammax*q[k]*(S*np.pi)**2*(1 - np.power(-1, k + 1))/((k + 1)*np.pi)

            # Third part
            Kt[i, j] += 6*Gammax*q[j]*(M*np.pi)**2*(1 - (-1)**S)/(S*np.pi)

    return Kt


# Compute freq/damping for Galerkin model
def freqDampGal(lamb, muM, nfreqs, Ksg, Kag, Cag, Zg, Ig):

    # Compute damping value
    gdamp = np.sqrt(muM*lamb)

    A = np.bmat([[Zg, Ig], [-2*lamb*Kag - 2*Ksg, -gdamp*Cag]])
    v = np.linalg.eigvals(A)[::2]
    inds = np.argsort(np.imag(v))

    v = v[inds[:nfreqs]]
    wd = np.abs(np.imag(v))
    n = np.real(v)
    #z = n/wd
    #z[wd == 0] = n[wd == 0]

    return wd/(2*np.pi), n


# Function to compute frequency/damping for the ROM
def freqDampRom(nfreqs, Omega, Ka, Ca, Z, I):
    # Obtain equivalent pressure and speed of sound
    As = np.bmat([[Z, I], [Ka - Omega, Ca]])
    v = np.linalg.eigvals(As)[::2]
    inds = np.argsort(np.imag(v))

    v = v[inds[:nfreqs]]
    wd = np.abs(np.imag(v))
    n = np.real(v)
    #z = n/wd
    #z = n/wd
    #z[wd == 0] = n[wd == 0]

    return wd/(2*np.pi), n


def criticalBoundariesStatic(lambdaVec, muM, Ks, Ka, Ca, Z, I, Gammax, fs, Ipi2, oneMinusOneM):
    '''Obtain flutter and divergence boundary (if it exists in the supplied range)
    in terms of mu/M values for given system matrices, considering the effects of static preload.'''

    n = len(lambdaVec)
    nfreqs = 4

    damping = np.zeros((n, nfreqs))
    freq = np.zeros((n, nfreqs))

    for ind, lamb in enumerate(lambdaVec):
        q0, w = staticDeflection(lamb, Gammax, Ks, Ka, fs, Ipi2, oneMinusOneM, nxi = 2)
        Kt = tangentStiffness(q0, Gammax)
        freq[ind, :], damping[ind, :] = freqDampGal(lamb, muM, nfreqs, Ks + Kt, Ka, Ca, Z, I)

    # Obtain flutter boundary
    maxDamping = np.max(damping, axis = 1)
    signChange = np.where(np.diff(np.sign(maxDamping)))[0]
    if len(signChange) == 0:
        flutter = None
    else:
        signChange = signChange[0]
        flutter = np.interp(0, maxDamping[signChange:(signChange + 2)],
                                   lambdaVec[signChange:(signChange + 2)])

    # Obtain divergence boundary
    minFreqs = np.min(freq, axis = 1)
    divIndex = np.where(minFreqs == 0)[0]
    if len(divIndex) == 0:
        divergence = None
    else:
        divIndex = divIndex[0]
        divergence = lambdaVec[divIndex] # This could be more accurate

    return flutter, divergence, freq, damping


def getlambda(filepath, mInf, gamma, a, De):
    with open(filepath, 'rt') as fid:
        for line in fid.readlines():
            key = line.split(':')[0]
            if key == 'freestreamPressure':
                p = float(line.split(':')[-1].strip('\n'))
    return p/setpInf(1, mInf, gamma, a, De)

#%% Static dynamics - Abaqus

# Fixed ROM parameters
filepath = r'../abaqus_data/deformed_dynamics'
datafile = 'data_WETTED_SURFACE.pkl'
inputfile = 'cosim.inp'
lambmax = 200.0
muM = 0.1
nm = 20
Z = np.zeros((nm, nm))
I = np.eye(nm)
nfreqs = 4


fnab = list()
fndryab = list()
zab = list()
lambvec = list()

# Variable parameters
hhvec = [1.0, 2.0, 3.5, 5.0]
filefun = lambda hh: os.path.join(filepath, ('hh_%.1f' % hh).replace('.', 'pt'))
lambdaFail = list()
for hh in hhvec:
    with open(os.path.join(filefun(hh), datafile), 'rb') as fid:
        data = pickle.load(fid)[0]
    N = len(data)

    # Pre-allocate result vectors
    fnab.append(np.zeros((N, nfreqs)))
    fndryab.append(np.zeros((N, nfreqs)))
    zab.append(np.zeros((N, nfreqs)))
    lambvec.append(np.zeros((N, )))

    # Find maximum pressure for this case
    lambmax = getlambda(os.path.join(filefun(hh), inputfile), mInf, gamma, Lx, De)
    for ind, system in enumerate(data):
        A = system['A']
        B = system['B']
        D = system['D']
        omega = system['omega'][:nm]
        phi = system['phi'][:, :nm]
        lamb = system['t']*lambmax
        lambvec[-1][ind] = lamb

        Omega = np.diag(omega**2)
        pInf = setpInf(lamb, mInf, gamma, Lx, De)
        aInf = setaInf(muM, pInf, mInf, Lx, gamma, rho, t)
        U = mInf*aInf
        stiffnessCoeff = pInf*gamma*mInf**2/np.sqrt(mInf**2 - 1)
        dampingCoeff = stiffnessCoeff*((mInf**2 - 2)/(mInf**2 - 1))/U

        Ka = stiffnessCoeff*phi.T @ D @ A @ phi # Full set of coefficients included
        Ca = dampingCoeff*phi.T @ D @ B @ phi # Full set of coefficients included

        fnab[-1][ind, :], zab[-1][ind, :] = freqDampRom(nfreqs, Omega, Ka, Ca, Z, I)
        fndryab[-1][ind, :] = omega[:nfreqs]/(2*np.pi)
    lambdaFail.append(lamb)

# TODO: Save the last step of each Abaqus run and use that as the stability boundary

#%% Static dynamics - Galerkin model

muM = 0.1
m = 6
Cag = np.eye(m)
Zg = np.zeros((m, m))
Ig = np.eye(m)
nfreqs = 4
N = 100
lambdaVec = np.linspace(0, 400, N)

fng = list()
zg = list()

for hh in hhvec:

    Gammax = 8*hh
    Ksg, Kag, fs, Ipi2, oneMinusOneM = prepVecMatrices(m, Gammax)
    fng.append(np.zeros((N, nfreqs)))
    zg.append(np.zeros((N, nfreqs)))

    for ind, lamb in enumerate(lambdaVec):

        q0, w = staticDeflection(lamb, Gammax, Ksg, Kag, fs, Ipi2, oneMinusOneM, nxi = 2)
        Kt = tangentStiffness(q0, Gammax)
        fng[-1][ind, :], zg[-1][ind, :] = freqDampGal(lamb, muM, nfreqs, Ksg + Kt, Kag, Cag, Zg, Ig)



raise Exception
#%% Plotting
# This one plots both sets of results
# Set up figure

# Some nicer colors
galerkinColor = (0.2, 0.2, 0.6, 1.0)
abaqusEdgeColor = (0.6, 0.2, 0.2, 1.0)
abaqusFaceColor = (0.6, 0.2, 0.2, 0.75)

fig, ax = plt.subplots(2, 2)
fig.set_figheight(9)
fig.set_figwidth(16)

ax = ax.flatten()

for i, hh in enumerate(hhvec):
    lineg = ax[i].plot(lambdaVec, fng[i]/tau2t, '-', color = galerkinColor)
    linea = ax[i].plot(lambvec[i], fnab[i], 'o', markeredgecolor = abaqusEdgeColor,
               markerfacecolor = abaqusFaceColor)
    lineg[0].set_label('Galerkin')
    linea[0].set_label('Abaqus/ROM')
    ax[i].set_title('H/h = %.1f' % hh, fontsize = 14, fontweight = 'bold')
    ax[i].set_xlim([0, 300])
    ax[i].set_ylim([0, 200])

ax[1].legend(fontsize = 12)
ax[0].set_ylabel('Natural Frequency [Hz]', fontsize = 14, fontweight = 'bold')
ax[2].set_ylabel('Natural Frequency [Hz]', fontsize = 14, fontweight = 'bold')

ax[2].set_xlabel('Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
ax[3].set_xlabel('Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')


helper.save(fig, 'images/large/fig07_deformed_freqs.svg', 8, 16)
# helper.save(fig, 'images/small/figXX_deformed_freqs.jpg', 5, 12)

#%% Also plot the damping values (not ratios)

# Some nicer colors
galerkinColor = (0.2, 0.2, 0.6, 1.0)
abaqusEdgeColor = (0.6, 0.2, 0.2, 1.0)
abaqusFaceColor = (0.6, 0.2, 0.2, 0.75)

fig, ax = plt.subplots(2, 2)
fig.set_figheight(9)
fig.set_figwidth(16)

ax = ax.flatten()

for i, hh in enumerate(hhvec):
    lineg = ax[i].plot(lambdaVec, np.max(zg[i]/tau2t, axis = 1), '.', color = galerkinColor)
    linea = ax[i].plot(lambvec[i], np.max(zab[i], axis = 1), 'o', markeredgecolor = abaqusEdgeColor,
               markerfacecolor = abaqusFaceColor)
    ax[i].hlines(0, 0, 300, 'k', linestyle = ':')
    ax[i].set_title('H/h = %.1f' % hh, fontsize = 14, fontweight = 'bold')
    lineg[0].set_label('Galerkin')
    linea[0].set_label('Abaqus/ROM')

    ax[i].set_xlim([0, 300])
    # ax[i].set_ylim([0, 200])

ax[1].legend(fontsize = 12)
ax[0].set_ylabel('Damping Factor n [Hz]', fontsize = 14, fontweight = 'bold')
ax[2].set_ylabel('Damping Factor n [Hz]', fontsize = 14, fontweight = 'bold')

ax[2].set_xlabel('Damping Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')
ax[3].set_xlabel('Dynamic Pressure $\mathbf{\lambda}$', fontsize = 14, fontweight = 'bold')

helper.save(fig, 'images/large/figXX_deformed_damp.png', 8, 16)
# helper.save(fig, 'images/large/figXX_deformed_freqs.png', 8, 16)
